/*

void 형태의 runner이면 exception이 생겼는지 알기가 힘들고, 오래 걸리는 task도 그렇다, 꼭 이것을 만들어 넣어서 전체 로그를 기록해 두도록 하자

*/

public class CustomAsyncExceptionHandler  implements AsyncUncaughtExceptionHandler {

    @Autowired
    private LogService logService;

    @Override
    public void handleUncaughtException(Throwable throwable, Method method, Object... obj) {

      //여기서 로그도 기록하고 보내자 .....
        //logService.log........
        System.out.println("Exception message - " + throwable.getMessage());
        System.out.println("Method name - " + method.getName());
        for (Object param : obj) {
            System.out.println("Parameter value - " + param);
        }
    }

}

/*

ThreadPoolTaskExecutor를 config할때 exceptionHandler도 넣어 주자
@Configuration
@EnableAsync
public class SpringAsyncConfig {

    @Bean(name = "threadPoolTaskExecutor")
    public Executor threadPoolTaskExecutor() {
        return new ThreadPoolTaskExecutor();
    }
}


출처: http://springboot.tistory.com/38 [스프링부트는 사랑입니다]

@Override
public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
    return new CustomAsyncExceptionHandler();
}


출처: http://springboot.tistory.com/38 [스프링부트는 사랑입니다]


*/
//출처: http://springboot.tistory.com/38 [스프링부트는 사랑입니다]
