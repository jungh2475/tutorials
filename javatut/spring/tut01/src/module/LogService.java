package com.kewtea.app;

/*

  LogService
    1)file
    2)console system.Print
    3)server _url

  파일을 쓰는것, url로 보내는 것 모두가 비동기적으로 이루어져야 한다. 그리고 많은 데이터를 처리하기 위해서 한번씩 쓰면 시스템에 무리가 가므로,
  ArrayList에 적어 놓으면 일괄적으로 모아서 처리하게 한다

  어느 class에서 문제가 있는지 보고하도록 해야 한다
  @Autowired LogService logService -. @PostConstruct Logger logger = logService.getLogger(className.class);
  logger안에 serviceName,classname,timestamp이 포함되어져서 알아서 보고가됨.  logger.log(String msg);

  console-spring: Logger logger = LoggerFactory.getLogger(WorkOrderController.class);

*/

//@PropertySource("classpath:/config.properties}")
@Service
public class LogService {

  //최대 100000까지 저장가능하고 그 이상은 덮어서 0부터 써서 저장한다.
  int LogListSize = 100000;
  int clearUnit = 10000;  //이만큼 모았다가 일괄 파일 저장 혹은 서버 통신을 한다.

  private ArrayList<Log> loglist;  //size? circular log 시스템임

  @Value("${log.filename:'spring.log'}")
  String lgFileName;
  @Value("${log.url:'http://api.kewtea.com/report'}")
  String lgUrl;
  @Value("${log.urltoken:'guest'}")
  String lgUrlToken;
  @Value("${log.crashdumpfilename:'crashdump.txt'}")
  String lgCrashDumpFileName;

  @Value("${listOfValues}")
  private String[] valuesArray;


  FileWriter fw;
  AsyncRestTemplate asyncRestTemplate;  //RestTemplate과 비슷하지만 다른 점이 있다면 callback 함수가 있다, onSuccess, onFailure
  //@Async
  //public

  /*
    normal inner : Outer.Inner inner = outer.new Inner(); inner.getNum()
    static inner:  Outer.Inner nested = new Outer.Inner(); nested.getPrint()
  public class Logger {

  }
  */

  public int log2file(){
    return -1;
  }

  private int log2cloud(){

    MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();

    ListenableFuture<ResponseEntity<Map>> entity = asyncRestTemplate.postForEntity(lgUrl, param, String.class);  //postForEntity
    entity.addCallback(result -> {
        System.out.println(result.getStatusCode());
        System.out.println(result.getBody());
    }, ex -> System.out.println(ex.getStackTrace()));

  }

  @PostConstruct
  public void init(){
    fw = new FileWriter(new File("textfile.txt"));
    //dump logfile이 혹시 있으면 이것을 보고하도록 한다
  }

}
