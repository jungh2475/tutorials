class BackupUtil {

    public String dir;
    public String gsBucket;
    //String(path) mysqldump(String filename)

    //mv

    public BackupUtil(String dir, String gsBucketName){
      this.dir=dir;
      this.gsBucket=gsBucketName;
    }

    public String backupCmdsFull(){
      //아래처럼 명령어들과 예상치들을 적어서 List로 만들어 낸다 
      return null;
    }

    public List<List<String>> buildCommands(String scenario){ //String(1)-cmd, (2)-expect, ..optional (3)errors //이러면 Map key String에 중복을 허락하지 않는데......
      //이함수는 switch로 다양한 명령 구문을 만들어 낸다. 밖에서 선언해서 가져다가 쓰는 것이 보기가 좋을듯함. 아니믄 mysql에 저장해서 가져 올까나?
      // new Gson().fromJson(configRecord, ConfigRecords.class); => List<List<String>>
      /*
      1.cd //해당 디렉토리로 이동
      2.mysqldump -u kewtea -p"SSSS" kew1db > filename.sql
      (3.optional set cpulimit, pid check-cpuprocess)
      4.check file_exist, size
      5.clean other old files inn that directory 최신것 2개정도만 남기고 다 삭제하자 -용얄절약을 위해
      6.copy to gsutil gstorage
      (7.optional- process progress check)
      */

      List<List<String>> commands_withexpects=new ArrayList<List<String>>();
      ArrayList<String> singleSet = new ArrayList<String>();
      //for loop to buildCommands
      singleSet.add("cmd1");
      singleSet.add("expect1");

      //mysqldump 하고 서버안에 최신것 몇개만 남기고 나머지는 삭제한다
      //다른 서버에 있는 mysql을 어떻게 backup하는가? ssh로 접속해서 해야하는가? vagrant로 만들어서 실험해 봐야 할듯,....
      String cmd = "mysqldump --all-databases -h localhost -u root -pmypwd > mydump.sql ";  //hostIp, u, pssword, filename

      commands_withexpects.add(singleSet);
      return commands_withexpects;
    }

    public String getFilename(long timenow){
      String filename="kew1-mysql.sql";
      return filename;
    }

}
