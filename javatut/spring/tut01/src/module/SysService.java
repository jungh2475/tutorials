//package com.kewtea.app.services

@Service
class SysService {

  /*
  오래 걸리는 것이 많은 경우 미리 메소드에 @Async를 붙이면 전체로 묶어서 실행하기가 힘들다.....
  이경우 sync로 method를 만들고 결과를 Future에 넣어서 전달 하면 어떨까?  중간에 진행상황들은 progress로 넣어 줄수가 있는가?
      (A) set who I am
      (B) system Reporter : via SysRestController.java (url: api/sys/report?from=&to=)
      (C) DB backup & file cleaner if you are admin(db) server
          db bakcup, snapshot & sys-records cleaning(일부 제거) 중 1)mysql file move는 차라리 python pexpect script로 처리하게 하는 것이 나은가?
          그러나, 2)snapshot & sys-records 클리닝은 spring repository로 처리하게 하는 것이 좋아보이는듯.....?

          python script는 pytut/devops/pexpect_run.py에 있음 실행할 명령은
          python3 pexpect_run.py backupfull 이라고 내 machine에서 해주면 그쪽에 ssh 연결을 해서 실행을 한다

          java expectj 및 jSch도 검토 해보았으나, python이 앞으로 재사용성등에 있어서 더 나아보임. 스프링서버 설치도 번거로움...
          https://wangchuanfa.wordpress.com/2010/04/27/java-expectj-sample-code/
  */

  @Autowired
  private SysRecordRepository sysRepository;
  private BackupUtil backupUtil;
  private ProcessRunnerUtil cmdRunner;

  @Value("${app.server.role:web}")   //db_server or admin, registry
  public String server_role;

  @Value("${app.db.backup.file_dir:/backup_files}")   //
  public String backupfile_dir;
  @Value("${app.db.backup.gstorage.bucket:kew1-bucket}")   //
  public String backup_bucket_name;
  @Value("${app.db.backup.interval:72}")   //backup 주기 by hr
  public int backup_interval;


  @PostConstruct
  void init(){
        //만약 server_role이 db관련 작업 권한이 있으면 주기적으로 db청소, 관리를 한다

        if(server_role.contains("admin")){
            backupUtil= new BackupUtil(backupfile_dir, backup_bucket_name);
            //inject all @repositories : journalRepo, naimRepo, sysRecords, userRecords(여기서는 마우스 동작 등 critical 하지 않은 것들만 제거 가능함....).....
        }

        cmdRunner= new ProcessRunnerUtil();
  }

  @Async
  public Future<List<String>> reportInstanceStatus(){
    //linux cmds를 돌려서 내 기계의 현재 상태, mysql 크기 및 db 크기, 잔여량을 알아다가 막 dump  해준다 .....
    AsyncResult<List<String>> report = null
    List<String> list = new ArrayList<>();
    list.add("dfsdfs");
    report= new AsyncResult(list);
    return report;
  }


  public List<SysRecords> getSysRecords(String[] conditions){
    List<SysRecords> results=new ArrayList<>();
    //데충 이런식으로 try...
    results = sysRepository.findByStringCondition(conditions);
    return results;
  }

  @Async  //sysAdmin으로 써 보고하는 전체 보고서 ....
  public Future<Map<String,String>> reportFull(long from, long to, String scope){  // key:value로 각종 보고를 함 혹은 Report or Record란 객체를 만들까?

    //scope가 overview이면 다른 서버들을 포함한 전체 상태를 보고한다
    //scope = errors 중요 문제들만 보고한다 , full이면 위의 모든것들을 합쳐서 보고한다

    //다른 서버의 ip 주소를 알아서 그들에게서도 정보를 받아 온다
    List<String> server_ips = new ArrayList<>();

    for(String ip in server_ips){
      //restTemplate로 접속해서 값을 읽어 온다 ....
    }

    return null;
  }


  public Future<long> backupImpl(List<List<String>> cmdswithexpects){

      /*
        1.mysqldump, 2.gstorage로 이동시킴  3.sysrecord에 내용 저장, 그리고 저장된 sysrecord id를 반환, 실패하면 -1
        성공하면 저장한 SysRecord id를 반환
        내기 딤딩지기 아니면 이 걸 그냥 bypass함
        ssh
        create filename
        (최소 5개의 cmds가 필요하다 )
        1.cd //해당 디렉토리로 이동
        2.mysqldump -u kewtea -p"SSSS" kew1db > filename.sql
        (3.optional set cpulimit, pid check-cpuprocess)
        4.check file_exist, size
        5.clean other old files inn that directory 최신것 2개정도만 남기고 다 삭제하자 -용얄절약을 위해
        6.copy to gsutil gstorage
        (7.optional- process progress check)
        make SysRecord for job completion
      */
      Session session = cmdRunner.getSshConnection("","","","",2000L);

      List<String> results = cmdRunner.run(cmdswithexpects);

  }

  @Schedule(cron="0 0 02 2,12,20 * ?")   //매월 2일,12,20일 새벽2시에 실행: (6) 초 분 시 일 월 요일
  @Aysnc
  public Future<Long> backup(){

    List<List<String>> cmdswithexpects = backupUtil.buildCommands("full-backup");
    //try로 감싸자
    ???? Future<Long> result_report_id=bakcupImpl(cmdswithexpects);
    return result_report_id;
  }

  /*
  @Async
  public Future<Long> restore(int mode, String name){  //mode=1 현재 파일명에서 mysql restore, mode=2 bucket filename을 가져와서 다시 복구 한다
    //적어만 두고 구현은 하지 말까? 실제로 복구를 하려면 ssh로 터미널 접속해서 수작업을 하도록 하는 편이 안전할까?

      commands : mysql -u kewtea -p"SSS" kew1db < data.sql


    Future long result =-1;

    //1.
    try{
    }catch(Exception e){
    }

    return result;
  }
  */


  public long cleanSnapshotSpace(int option){
    return -1L;
  }

//여기만 @Schedule을 넣어서 한곳에서 주기적으로 할 일들을 관리하게 하는 거싱 좋을 듯......
  @Schedule(cron="0 0 02 2,12,20 * ?")   //매월 2일,12,20일 새벽2시에 실행: (6) 초 분 시 일 월 요일
  @Aysnc
  public Future<Long> housekeeping(){
    Future<Long> result_report_id;
    //new AsyncResult<Long>(value);
    return result_report_id;
  }

}

/*

(A)내가 어떤 종류의 instance로 동작하느냐에 따라 하위 서비스들이 설정되게 하자

    1)web server front : sysmonitoring  기능 및  SnapshotService
    2)db server : 위의 기능외에 db backup and restore 기능 (gStorage bucket에 저장 )
    3)superadmin server (db server안에 있을 수도 있고 모자르면 나올수도 있다) 위의 모든 기능들 필요 , 다른 서버들의 상태로 확인함

(B) SystemReporter
그리고 모든 서버들에는 rest api가 있어서 보고서를 받아 올수가 있다

import LoggingService;
import SnapshotService;  //주기적으로 오래된 자료를 지워주어서 청소하기
import MailService;



 sys-rest api
    이 녀석을 통해서 원하는 자료들을 가져가게 한다
  get_report (period)
  get_reprot_now : 현재 snapshot

    그리고 내부적으로는 SysMonitorUtil, BackUpRestoreUtil을 각각 instantiation해서 서비스를 제공한다
    본인이 일반 webserver로 생성이 되면 backuprestoreutil 기능을 construct하지 말것,
    본인이 mysql db 서버 혹은 superadmin server로 생성이 된 경우만 BackUpRestoreUtil을 추가 할것 ....


(C) db and file cleaner
        이런 행동을 dbServer가 들어 있는 instance에서만 가능하지 않은가

      - mysql backup file mysqldump ,
          외부에서 ssh로 접근해서 db_file 만들기가 가능한가? (할수는 있지만 일단은 보안상 하지 말자?)
          filename 만들기
      - copy to google Storage
      - restore db from local mysql_file or copy_from gStorage하고서 복구, restart_mysql
      - mysql안에 있는 snapshot들 정리 하기 : db_cleanser
          1)sysRecords, userRecords, sanpshots in contentJournal, commentJournal, naim,.....
          2)시간순서상, 중요도상에 덜 중요한것들을 추려내는 로직 구현..필요 시간(time_long) :
              application.properties => app.db.cleanser.offset-sysrecords=1000 //최근 1000개만 남기기?
                                        app.db.cleanser.keep=   //암만 오래되도 이런것들은 삭제하지 말것
                                        r

      - 로그 파일들 청소, zip, copy to gStorage ,..합치기?



*/


@Service
public class SysService {

  private SysMonitorUril sysMonUtil;
  private BackUpRestoreUtil BackUpRestoreUtil;


  @Async
  public CompletableFuture<Result> getMonitorReport(){

    String result = sysMonUtil.readSystemParameters();
    return null;
  }


  //load()//interface StorageObject implements 1)init(), 2)put(local-from, destination-to), 3)pull(destination-from, local-to)


}




//util modules  매장POP PF1050BB 충전식 아크릴 박스타입 IPS패널, 9/12, 9/13
//




class SysMonitorUtil {
  /*
   읽어야 할 내용 : cpu_load, mempry_ram, sql_size, error_logs(file),
   계산할 내용: 하루 transactions 숫자, 응닶간 평균, peak, ddos? 나중에 확장할수 있도록 .....

   linux commands로 시스템 상태 값들을 읽어와서 mysql 혹은 memory에 간직하고 있어야 한다
   (중요한 순서부터 일부만 구현하고, config-application.properties에서 설정하게 하자)

   이 모든 것들을 python pexpect, pxssh로 하는 것이 나은가? 아니면 java에서 하게 해야 하는가?
   (java method내에서의 동작 성능 측정은 java로 그 외의 외부 linux 명령어로 얻는 것은 되도록이면 python 서비스로 가져오자 )

    memory usage
    내가 접속한 db의 크기 읽기
    response 시간 읽기

    top : PID, USER, PR, %CPU %MEM TIME CMD
    vmstat: virtual mem statistics
    LSOF (list open files)
    NetStat
    fdisk, sfdisk, df
    결과 값을 grep, awk,sed,regx로 가져온다
    http://matt.might.net/articles/sculpting-text/

    https://stackoverflow.com/questions/47177/how-do-i-monitor-the-computers-cpu-memory-and-disk-usage-in-java

    Properties systemProps = System.getProperties();
        Set<Entry<Object, Object>> sets = systemProps.entrySet();
        https://www.daniweb.com/programming/software-development/threads/20292/how-to-find-system-info-through-java
    https://www.youtube.com/watch?v=sj1m_uzPJHU

  */


  public String readSystemParameters(){

    return null;
  }

  public SysMonitorUtil(){

  }

}

class BackupRestoreUtil {
  /*
    매일 mysql 파일 backup: 최신것 1개는 현재 machine에 덮어 쓰고, 또한 google storage bucket에 저장한다
    (거기도 용량이 넘치면 과거의 매월 1개로 최근 6개월어치를  저장해 둔다?)
    그리고 가끔씩 수작업으로 그 파일은 usb로 사람이 옮겨 적는다
    elasticsearch도 포함? -> 아니 이건 따로 하자 .....
    file bakcup을 하면서 서비스 성능 저하가 나지 않토록 linux cmd nice로 천천히 동작하게 한다?

    restore는 mysql_server에서 직접해주면 된다.

    https://cloud.google.com/storage/transfer/create-client
    https://cloud.google.com/storage/docs/quickstart-gsutil
    전송하는 파일의 크기가 커짐으로 인해 생기는 문제 및 resume transfer는?


    https://cloud.google.com/java/samples
    https://github.com/GoogleCloudPlatform/java-docs-samples/tree/master/compute/cmdline

    밖의 process에서 돌리는 것이 더 좋다면 linux process async로 돌리는 것이 나은가?
    make bucket : gsutil mb gs://[YOUR-BUCKET-NAME]

  */


  public Future<Result> put(String local, String destination) throws InterruptedException{
    return null;
  }


  /*

    Should you use gsutil or Cloud Storage Transfer Service?
    상관없음

    gsutil이 설치되어져 있는지 확인이필요함. gcloud compute안에는 이미 설치가 되어져 있음
    접근 권한은 Google Compute Engine instance,
    authenticates using the default Compute Engine service account associated with the project.
    https://cloud.google.com/compute/docs/access/create-enable-service-accounts-for-instances#using
    put만 할수 있도록하고 remove는 mysql instance 서버에서만 하도록 한다?

    gsutil ls gs://my-awesome-bucket
    gsutil ls -l gs://my-awesome-bucket/cloud-storage-logo.png
    gsutil mb gs://my-awesome-bucket/  만들기
    (put) gsutil cp Desktop/cloud-storage-logo.png gs://my-awesome-bucket
    (get) gsutil cp gs://my-awesome-bucket/cloud-storage-logo.png Desktop
    (delete) gsutil rm gs://my-awesome-bucket/cloud-storage-logo.png
    gsutil acl ch -u user@gmail.com:W gs://my-awesome-bucket 권한

    maven으로 client-lib로 java-api로 하던가, linux_subprocess로 명령어를 돌리던가...
        gcloud auth application-default login
        <dependency><groupId>com.google.cloud<artifactId>google-cloud-storage</artifactId><version>1.4.0</version>
        Storage storage, String bucketName
  */


  public CompletableFuture<Result> get(String destination-from, String local-to) throws InterruptedException{

      String cmd = "nice -n 10 ";  //
      cmd(cmd);
  }

  public CompletableFuture<Result> cmd(String cmd){



    //bucket creation: gsutil mb gs://my-awesome-bucket/

    StringBuffer output = new StringBuffer();

    Result result;
    //String cmd = "nice -n 10 ls -al";
    Process pr;
    try {
      Runtime run = Runtime.getRuntime(); pr = run.exec(cmd);
      //pr = Runtime.getRuntime().exec(cmd);
      pr.waitFor();
      BufferedReader reader = new BufferedReader(new InputStreamReader(pr.getInputStream()));
      String line = "";
  		while ((line = reader.readLine())!= null) {
  				output.append(line + "\n");
  		}

    } catch(Exception e){

    }


    //
    return CompletableFuture.completedFuture(result);;
  }


  //constructor
  public BackupRestoreUtil(StorageObject so){

      //Config = google storage
  }

}
