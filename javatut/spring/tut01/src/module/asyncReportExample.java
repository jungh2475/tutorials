//http://www.baeldung.com/spring-async
@Service
class ReportExampleService {

  @Autowired
  SysRecordRepository sysRepo;

  @Async
  public Future<String> dostuffshort1(){
    AsyncResult<String> result = new AsyncResult<>();
    //do something.....when ..done result
    return result;
  }



  @Async
  public Future<String> dostufflong(String ticketNumber){

    //https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/scheduling/annotation/AsyncResult.html
    try{
      //sleep 10seconds....
      String result="sldjkfslkdjlskjdf";
      return new AsyncResult<String>(result);
    }catch(InterruptedException e){
      //log에 기록함 .....
    }
    //try-catch로 감싸지 않아도 에러가 나면 @AsyncConfig에서 Exception을 잡아서 보고를 하게 해주어야 한다.

    return null;
  }




}

@RestController
class controllerExample {

  @Autowired
  ReportExampleService service;

  @RequestMapping //중간 결과물들을 동시에 요청해서 완료가 되면 다 합쳐서 최종 결과를 만들어 내는 경우 - 한번의 요청 시간 정도로 전체 수행이 가능 io과련된 작업인 경우 ...
  String collectAllResults(){
    StringBuffer sb= new StringBuffer("");
    //https://slipp.net/questions/271, StringBuffer는 각 메소드 별로 synchronized keyword가 존재
    AsyncFuture<String> future1=service.dostuff1();
    AsyncFuture<String> future2=service.dostuff2();
    while(true){
      if(future1.isDone) sb.append(future1.get());
      if(future2.isDone) sb.append(future2.get());
      //시간이 초과되었거나, 둘다 결과를 받앗다면 루프를 탈출한다 - break or return sb
      String result=sb.toString();
    }
    return result;
  }

  //먼져 ticket을 받아가고 일이 다되었는지 확인하여 결과를 받아 간다...
  @RequestMapping("/getTicket")  //post
  String requestJobAndGetTicket(){
    String ticketNumber;
    ticketNumber=service.registerJob();
    service.dostufflong(ticketNumber);
    //AsyncResult<String> future_result=service.dostufflong(ticketNumber);
    return ticketNumber;
  }

  //ticket관련된 일이 끈났는지 혹은 진행중인지 알려 주는 메소드
  @RequestMapping("/ticketStatus/{ticketNumber}")
  String getWorkStatusByTiket(String ticketNumber){
    String status="in-progress"; //done, in-progress, cancelled, error-cancel
    return status;
  }

  @RequestMapping("/getResultByTicket/{ticketNumber}")
  String getResultByTicket(String ticketNumber){

    String result=service.getResultByTicket(ticketNumber);
    //만약에 일처리가 안되어져 있다면 값이 없을 것이고, 다시 요청해야 할것임...혹은 진행중이라고 표시하도록 할수도 있음.
    return result;
  }


}


@Configuration
@EnableAsync
public class SpringAsyncConfig implements AsyncConfigurer {
//두가지 할일이 있다, threadpoolexecutor의 크기(최대 대기, 동시종작 --applications.properties에서 가져오기, 없으면 default를 사용) 를 정하고, thread에서 에러가 난경우 이를 catch해서 에러처리하고 sysrecord에 보고하기
  @Override
    public Executor getAsyncExecutor() {
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("async-%d").build();
        return CompletableExecutors.completable(Executors.newFixedThreadPool(10, threadFactory));
    }

  @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return (ex, method, params) -> logger.error("Uncaught async error", ex);
    }

}
