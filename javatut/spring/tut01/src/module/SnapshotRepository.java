

interface Snapshot {

  private ContentRepository;
  private SnapshotRepository;

  //crud: create(해당없음), read(BySnapshot꼭 추가), update(해당됨), delete(해당됨)

  long update

  long undo(long id);



}

interface ContentRepository

class SnapshotRepository {

    private JpaRepository jpaRepository;
    private Content content;
    @Autowired
    private SnapshotRepository;
    int mode=1;

    public SnapshotRepository(JpaRepository jpaRepo, Content content, int mode){
      this.jpaRepository=jpaRepo;
      this.content=content;
      this.mode=mode;
    }

    //save
    long save(Content content){
      long content_id =-1L;
      if(content.id.exist)//이런식으로 insert case면 그냥 실행
      { //복사본을 만들고 snapshot table에도 기록해둠
        long copy_id=jpaRepository.save(jpaRepository.findOneById(content.id));
        Snapshot snapshot = new Snapshot('journal',content.id,copy_id); //serviceName,ori_id, copy_id
        SnapshotRepository.save(snapshot);
        content_id=jpaRepository.save(content); //************?이것이 될것인가? 원래는 .save(JournalContent)이어야 하는데......
        //여기서 형변환을 해주어야 하는가? content= (JournalContent) content;
      }
      else {new Content = savedContent; savedContent=jpaRepository.save(content); content_id = savedContent.id;}
      return content_id;
    }


    //delete
    int delete(long content_id){
      int result =-1;
      if(mode==1){
        Content content;
        content=jpaRepository.findOneById(content_id);
        content.snapshot=1;
        jpaRepository.save(content);
        //그리고 snapshot table에 본인 것도 같이 기록? serviceName,ori_id, copy_id
      }else{jpaRepository.delete(content_id); result =1;}
      return result;
    }

    //undo
    //undo(long content_id)  //가장 최근 것 하나전으로 이동
    //undo(long content_id, int backsteps) //현재 원본에서 몇번째 전의 것으로 이동
    //undo(long content_id, long snapshot_content_id)  //snapshot id를 알경우 이것으로 직접 치환, 이것이 가장 원시적인 기본 구현일것임.

    //redo(long content_id)  //바로 전의 것의 상태로 되돌림. ....

}

//

class JournalService implements Snapshot {

    @Autowired
    private JournalContentRepository;
    @Autowired
    private SnapshotRepository;

    private SnapshotRepository;

    @Value("${app.journal.snapshotmode:1}")
    int snaphoteMode;

    @PostConstruct
    void init(){
      this.SnapshotRepository=new SnapshotRepository(JournalContentRepository, JournalContent, snapshotMode);
    }


    //method implementations
    long set(JournalContent content){
      long content_id=-1;
      if(content.id.exist){ 그냥 insert_save임}
      else{
        content_id=SnapshotRepository.save(content);
      }
      return content_id;
    }


}
