


class RepositoryWrapper<T, R> {
    private T content;
    private R contentRepo;//JpaRepository jpaRepository;
    private SnapshotRepository snapshotRepo;
    private int mode=snapshotMode;

    public RepositoryWrapper<T, R>(T t, R contentRepo,SnapshotRepository snapshotRepo, int snapshotMode){
      this.content = t;
      this.contentRepo=contentRepo;
      this.snapshotRepo=snapshotRepo;
      this.mode=snapshotMode;
      //is content part of JpaRepository<Content, Long>? 어떻게 체크 하지?
    }

    //save
    T save(T content){
      T result=null;
      result=contentRepo.save(content);
      return result;
    }

    //delete


    //undo


    //redo

    //listSnapshots₩ ㅠ


}


/*
JournalService에서 사용법은 아래와 같다


*/

class JournalService {

  private RepositoryWrapper<ContentJournal,JournalContentRepository> repositoryWrapper;
  @Autowired
  private JournalContentRepository contentRepo;
  @Autowired
  private SnapshotRepository snapshotRepo;
  @Value("${app.journal.snapshotmode:1}")
  int snaphoteMode;

  @PostConstruct
  void init(){
      repositoryWrapper = new RepositoryWrapper<ContentJournal,JournalContentRepository>(new ContentJournal(),contentRepo, snapshotRepo,snaphoteMode);


  }

  //set, delete일때만 repoWrapper를 쓰고 나머지  read일때는 꼭 snapshot flag를 넣어서 찾아 보게 한다

}
