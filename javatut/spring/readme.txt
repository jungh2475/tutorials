

spring starter
  set JAVA_HOME, in MacOSX
  maven -> mvn spring-boot:run, mvn package -> java -jar app.jar --port=8081 &
  pom.xml
    dependencies :
    application.properties
  eclipse - import project from maven
  junit test in eclipse & cmd (test runner)

Spring Test & Depolyment
  deployment active profiles : local, dev, prod1, prod2
      java -jar app.jar --Dspring.profiles.active=local --port=8081 &
      mvn spring-boot:run 도 같은 식으로
  testing:
      1)junit, mock, spring integration test(when.then),
      (junit test들에는 spring @붙이지 말것)()
      2)최종 기본 릴리즈용 검증은 @SpringBootTest
      3)@ActiveProfiles("dbprofile") 해당 active.profile일때만 실행 된다.
      (properties = "spring.profiles.include=testSpringProfile")


  logging & log files

spring components
  @Controller(model->list), @RestController
  @Service
  interface @Repository
  Model(@Column, @Valid, )
  @Bean, Config(@Configuration), @Component
  OOP - @PostConstruct: https://medium.com/@dmarko484/spring-boot-startup-init-through-postconstruct-765b5a5c1d29

Spring multiple databases
  - two mysql, 1 elasticsearch(2.4)
  - local install, vagrant-vbox-vms, gcloud installs
  - loading data by app.start.listener, backup,snapshot&restore, test-listener(before)

Spring Annotations Extra
  @Value, @Properties,@PropertySource("classpath:/test.properties")


Spring Cloud
  - proxy : netflix zuul
  - microservice farm: eureka discovery, feign client,
  - tracing
