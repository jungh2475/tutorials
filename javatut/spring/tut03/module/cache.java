//import


/*

  varnish-apache cache server
  spring services main cache
  spring cache method level

*/
@Service
class JournalService {

  private CacheWrapper cacheWrapper;  //2분 min마다 expire 시키자
  private List<Content> contents;
  @Autowired
  ConetntRepository contentRepo;

  @Cacheable(value="contents", key="#id", condition="#id!='23'" , unless="#result==null")
  public Contet getContentById(long id){
    //return contents.get(id);
    return contentRepo.findById(id);
  }

  @CachePut(value = "products", key = "#product.name" , unless="#result==null")
    public Product updateProduct(Product product) {
        logger.info("<!----------Entering updateProduct ------------------->");
        for(Product p : products){
            if(p.getName().equalsIgnoreCase(product.getName()))
                p.setPrice(product.getPrice());
                return p;
        }
        return null;
  }

  //@Schedule(* 20 * * * *)
  @Scheduled(fixedDelay = 120000) //or fixedDelayString = "${couchbase.cache.flush.fixed.delay}"
  @CacheEvict(value = "products", allEntries = true)
    public void refreshAllProducts() {}
}

//@CacheConfig(cacheNames={"addresses"})
class cache-spring-method {


  //@Cacheable(value="messagecache", key="#id", condition="id < 10") @CacheEvict
  @Cacheable("addresses")
  public String getAddress(Customer customer) {return null;}



}

@Configuration
@EnableCaching
public class CachingConfig {

  @Bean
  public CacheManager cacheManager() {
      return new ConcurrentMapCacheManager("addresses");
  }
    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(Arrays.asList(
          new ConcurrentMapCache("directory"),
          new ConcurrentMapCache("addresses")));
        return cacheManager;
    }
}
