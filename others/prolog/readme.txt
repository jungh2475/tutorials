[install]
  swi-prolog or gnu prolog
  [mac osx] brew search prolog
            brew install swi-prolog or brew cask install swi-prolog , latest version : brew install swi-prolog --HEAD
  [ubuntu]
      sudo add-apt-repository ppa:swi-prolog/stable
      apt-get update
      sudo apt-get install swi-prolog

[run]
swipl
swipl -s program.pl.
swipl -s load.pl -g go -t halt # start the application using the entry point go/0

%comment
[A]=[abc].

help(help).
likes(sam, X).
father(me,sarah).
father(X,Y) :- male(X), parent(X,Y).
?- father(me,X).
halt.

[user].  /* loading a file, user.pl  */
['file.pl'].           /* 1. Load a program from a local file*/

---------- prolog editor -----------
edit(file('father.pl')).
% facts:
male('Nat King Cole').
parent( 'Nat King Cole', 'Nathalie Cole').
father(me,sarah).
% rules :
father(X,Y) :- male(X), parent(X,Y).

factorial(0,1).
