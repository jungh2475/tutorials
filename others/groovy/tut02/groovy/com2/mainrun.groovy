

//import Person

//java class: javac -cp .:"/Applications/Contents/lib/*" SetTest.java
//groovyc model1.groovy -> model1.class -> groovy -cp . mainrun.groovy

def p = new Person();
p.age=17
p.name='john smith'
//or Person person = new Person(name:”Tomas Malmsten”, age:35)
//or def createNewPerson(name, age) {    new Person(name:name, age:age)}

println p.getName()



def map = [“one”:1, “two”:2, “three”:3]
Map<String, String> things = ['hello': 'world']

// Write to a file
new File("hello.txt") << 'Hello world!'
int sum = a + b
println "$sum ${sum.class}" // 42 class java.lang.Integer
List<Number> numbers = [-2, 12, 6, 3]
// Closures
def result = numbers
        .findAll { it > 0 } // filter
        .collect { it * 2 } // map
        .sum() // reduce
