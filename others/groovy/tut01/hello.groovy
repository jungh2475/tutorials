//import ....

println "hello, world"
this.args.each{ arg -> println "hello, ${arg}"}
/*
for (arg in this.args ) { //instead this.args.each{ arg -> println "hello, ${arg}"}
    println "Argument:" + arg;
}
*/


def x = 1
assert x == 2
