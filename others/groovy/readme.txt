
1)install
groovy(2.4,2.5)를 설치하려면 java가 동작하여야 한다
groovy -version
http://groovy-lang.org/learn.html

linux & ubunutu
1)using sdkman
  curl -s get.sdkman.io | bash
  source "$HOME/.sdkman/bin/sdkman-init.sh"
  sdk install groovy
  sdk upgrade groovy


mac osx
1)using sdkman
2)using brew
  brew install groovy
  echo "export GROOVY_HOME=/usr/local/opt/groovy/libexec" | tee -a ~/.bash_profile; source ~/.bash_profile


//hello.groovy:(run) groovy hello.groovy MyName yourName HisName
  println "hello, world"
  for (arg in this.args ) { //instead this.args.each{ arg -> println "hello, ${arg}"}
      println "Argument:" + arg;
  }

-------------------------------------------
2)compile if needed
groovyc ./com/something/A.groovy ./com/something/B.groovy

3)import java & subdir

  groovyc Hello1.groovy -> classpath에는 jar, class, zip 파일들만 인식되고 java.groovy는 인식이 안된다 
  반드시 컴파일 한후에 import 해서 쓸것 . 같은 디렉토리인 경우 groovy -cp . test.groovy

  import pacakge, include jar
    - groovy -cp mysql.jar status.groovy
    - this.getClass().classLoader.rootLoader.addURL(new File("file.jar").toURL())
    - Groovy grapes: @Grab(group='com.google.collections', module='google-collections', version='1.0')
    - add jars at $HOME/.groovy/lib
    - run as bashscript: add the first line: #!/usr/bin/env groovy -cp ojdbc5.jar

def x = 1
assert x == 2
-------------------------------------------
4)using gradle

  (linux) dl gradle-2.11-all.zip ->sudo mv gradle-2.11 /opt/gradle->export ORIENT_HOME = /opt/gradle
  gradle –v
  (mac osx) brew install gradle

  먼져 maven project를 만들고 gradle로 convert: mvn archetype:generate -> mvn test
  gradle setupBuild  -> build.gradle 파일을 만듬
  gradle tasks  //build, clean, help, install, test

  echo ‘export PATH=$GRADLE_HOME/bin:$PATH’ >> ~/.profile

------------------------------------------

5)fast testing java with groovy

  (1) make variables faster with groovy : list, map
    - old java way

    - new groovy way
        Map<String, String> things = ['hello': 'world']
        List<Number> numbers = [-2, 12, 6, 3]
        new File("hello.txt") << 'Hello world!'
        println "$sum ${sum.class}"
        println "This answer to life, universe and everything: ${result}"

        advanced (when numbers is list)
        def result = numbers.findAll { it > 0 }.collect { it * 2 }.sum() // filter->map->reduce

  (3) unit test faster with spock
    여러가지 숫자들을 넣어서 한테스트를 수행할때 좋음.
