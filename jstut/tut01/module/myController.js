//var util2 = require("./util2");
// logger를 달자
//var dataService = require("./dataService");

function myController($scope, dataService, Logger, subs){
  // subs > (1.util2 + 2.x + 3.y )

  //1.inner variable & methods
  var result = {};
  var sub1 = function(){
    result = "hi i am john";
  };
  var successCall = function(response){
    var temp = subs.util2(23);  //+response
    $scope.display = temp;
  };

  var errorCall = function(error){
    Logger.log(error);
  };

  //2.$scope
  /*
      - ui actions,
      - 관리+mapping 화면 변수들 : title, url, left_clikc_link, right_click_link, comments, datePage, userName,
      - (comment 객체 정의는 ? comment > (time, bodytext, collection_id, keywords, .....,ver))
  */
  //$scope.result = "1";
  $scope.clickme1 = function(event){
    dataService.getContentById(3).then(successCall, errorCall);
  };
  $scope.clickme2 = function(){
    sub1;
    //$scope.$apply();
    $scope.display = result;
  };

  //3.broadcast receiver
  $scope.$on();

}

module.exports = myController;
