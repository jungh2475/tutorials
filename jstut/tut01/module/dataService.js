//require
var util1 = require('./util1');


function dataService($http, $rootScope, Logger, subs){

  //1. variables

  //2. inner functions & variables;
  //캐시도 밖으로 꺼내서 거기서 정의를 내려서 여기에 주입해야 한다
  var temp = new util1();
  //var temp2 = util1.....

  var cache = {
    comments:"aaa",
    getComments:function(){
      return new Promise(function(resolve,reject){
        resolve(comments);
      });
    }

  };


  var rootBroadcast = function(eventName, data){
    $rootScope.$broadcast(eventName, data);
    //$rootScope.$broadcast('greeting', message); -> $scope.$on('greeting',function(event,data){......});
  };


  var url = "http://192.168.0.23";
  //여기에서는 서버하고 통신하고 싶은 앞부분 내역들을 function 형태로 적어서 나중에 inPromise로 실행 할수 있도록 준비 해 둔다
  var innerPromise = function(){return $http.get(url);}; //construction할때 실행 되지 않토록 function으로 감쌈
  //var innerPromise = $http.get(url);  //이렇게 밖으로 빼둘수는 없음. 대신에 .... 아래처럼 method안에 넣어 두어야 함.
  /*
  var successCall = function(response){
    console.log("successCall!");
    resolve(response);
  };
  var errorCall = function(error){
    console.log("errorCall!");
    reject(error);
  };
  */

  //3. public methods

  this.getContentById = function(id){
      //innerOperation(id).then(;)
      //var inPromise = $http.get(url);
      var inPromise = innerPromise();
    return new Promise(function(resolve,reject){
      inPromise.then(function(response){  //innerPromise => $http.get(url)
        //resolve(response.data);
        console.log("i have response");
        resolve(response);
      },function(error){
        console.log("i dont have response");
        reject(error);
      });
    });
  };


  this.getContentById2 = function(id){
    var input = url+"/"+id;
    var inPromise = innerPromise();
    var promise = new Promise(function(resolve,reject){
      //innerPromise(input).then(success->resolve, error->reject(response));
      //inPromise.then(successCall, errorCall);
      inPromise.then(function(response){console.log("method2-ok"); resolve(response);},function(error){console.log("method2-fail"); reject(error);});

    });
    return promise;
  };


  this.testBroadcast = function(msg){
    var inPromise = innerPromise();
    var promise = new Promise(function(resolve,reject){
      //
      resolve(msg); //reject(msg);
    });
    inPromise.then(function(response){rootBroadcast('comments-updated',response);},function(error){console.log("error-bcst");});
    return promise;
  };

}

module.exports = dataService;


/*
  promise tutorial : https://davidwalsh.name/promises ,
    1) pattern

        var promise = new Promise(function(resolve,reject){
          //subFunctionPromise.then(do-ok ->resolve(response.data), //not-ok -> reject(error))
          // or ...then().catch......
        });
    2) promise all
        var request1 = $http.get(url); //promise
        var promise = Promise.all([request1,request2]);

    3) es6 promise
        var p = new Promise((resolve, reject) => resolve(5));
        p.then((val) => console.log("fulfilled:", val),(err) => console.log("rejected: ", err));
        var p = Promise.resolve(42);
        http://www.datchley.name/es6-promises/ 
*/
