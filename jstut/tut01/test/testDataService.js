var dataService = require('../module/dataService');
//var $http = require('axios');



function testDataService(){
  console.log('strating....');
  var http = {
    get:function(url){
      return new Promise(
        function(resolve,reject){ resolve(""+url+":i am happy");}
      );
    }};
  var rootScope = {};
  var Logger = {};
  var subs = {};
  var dService = {};

  this.init = function(){
    console.log("init");
    dService = new dataService(http, rootScope,Logger, subs);
    console.log("init_completed");
  };
  init();
  //var dService = dataService($http,$rootScope) //$http, $rootScope, Logger, subs

  //testcase 1 : getContentById with dummy http
  if(dService !=null){
    var promise = dService.getContentById(45);
    promise.then(
      function(response){
        console.log(response)
      }, function(error){ console.log(error);}
    );
  } else { console.log("dService is null");}

}

function testDataService2(){
  var axios = require('axios');
  //npm install -g axios, export NODE_PATH=/opt/lib/node_modules, (osx) /usr/local/lib/node_modules

  var rootScope = {};
  var Logger = {};
  var subs = {};
  var dService; // = {};


  this.init = function(){
    console.log("init");
    dService = new dataService(axios, rootScope, Logger, subs);
    console.log("init_completed");
  };
  init();

  //testcase2 : getContentById with axios
  console.log("test2.1 is now starting...");
  dService.getContentById(66).then(
    function(response){ console.log(response); },
    function(error){
      console.log("printing error");  //404 no such thing
      //console.log(error);  //error is too long.....
  });

  console.log("test2.2 is now starting...");
  dService.getContentById2(55).then(
    function(response){console.log("success");}, function(error){console.log("error-404");}
  );

  console.log("test2.3 -$broadcasting is now starting...");
  dService.testBroadcast("hihi").then();
  /*
  dService.testBroadcast("hi i am broadcasting").then(function(response){console.log("msg fired!");});
  var $scope = {}; //$scope.$on로 실험을 node에서는 할수가 없다 ..........
  $scope.$on('comments-updated', function(data){ console.log("received broadcast msg");});
  */
}


//main run
//testDataService();
testDataService2();
