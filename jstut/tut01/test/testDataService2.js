var events = require('events');
var eventEmitter = new events.EventEmitter();

//emitter...addListener, .on,....listeners, ...emit('evnetName')
//https://www.tutorialspoint.com/nodejs/nodejs_event_emitter.htm, https://www.sitepoint.com/nodejs-events-and-eventemitter/ http://bcho.tistory.com/885

//jasmine using inject: https://stackoverflow.com/questions/16757503/how-can-i-test-broadcast-event-in-angularjs
//expect($rootScope.$broadcast.calledWith("testEvent")).to.be.true
//https://stackoverflow.com/questions/24314885/how-can-i-test-a-scope-on-event-in-a-jasmine-test
//ng_mock : http://www.bradoncode.com/blog/2015/07/08/unit-testing-scope-ngmock/ 
var rootScope;
beforeEach(inject(function($injector) {
    rootScope = $injector.get('$rootScope');
    spyOn(rootScope, '$broadcast');
}));
