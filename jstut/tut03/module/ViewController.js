function ViewController($scope, DataService, Logger, Subs){   //DataService, Logger, Subs는 우리가 만들어 넣어야함.

  //1. constructor


  //2. vairables


  //3. inner methods

  //적용은 반드시 $scope.$apply(fn=function(){$scope.value=myValue;});, http://www.nextree.co.kr/p8890/
  this.scope_apply = function (apply){
    //apply - getKeys -> $scope.apply();
    $scope.$apply(function(){
      //$scope.//apply
      for(key in Object.keys(apply)){
        $scope.key=apply.key;  //? 이게 될까?
      }
    });
  };

  this.buildPage1 = function (){
    var p1 = DataService.getContentById();
    var p2 = DataService.getContentById();
    Promise.all([p1,p2]).then(function(values){
      //$scope.x=values.x;
      $scope.$apply(function () {
        $scope.x=values.x;
      });
    });
  };

  this.buildPage2 = function (){
    var p1 = DataService.getContentById();
    var p2 = DataService.getContentById();
    Promise.all([p1,p2]).then(function(values){
      scope_apply(values);
    });
  };

  //4.$scope. variables & methods(click)

  $scope.leftClick = function (){};  //=moveLeft(), ng-click='leftClick()'

  /*
    왼쪽클릭, 오른쪽 클릭, 메뉴클릭_toggle, timelineClick,

  */


  //5.$scope.$on ($broadcast event listeners)



}


module.exports = ViewController;
