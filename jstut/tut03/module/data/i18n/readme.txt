

https://scotch.io/tutorials/internationalization-of-angularjs-applications
https://medium.com/@t_tsonev/making-sense-of-angular-internationalization-i18n-e7b26fb9c587
(참고 linux, android i18n, chrome extension) http://lxstitch.tistory.com/57

그리고 character escape, url encode에 대해서도 확인해 보자 - 왜 필요한지...java,javascript, python, php는 각각 어떻게 다른가?

다국어 메뉴(i18n) 지원
     - 일하는 순서 : ui 검토 -> crx -> spring -> angular
     - browser/chrome에서 detection하는 것 보다, localStorage에 써넣은 값이 우선함, url 변동은 없음
     - crx : _locales/en/messages.json에 적는다, default=en in manifest.json, var message = chrome.i18n.getMessage("click_here", ["....소스: https://developer.chrome.com/extensions/i18n
     - spring : application.properties에 spring.messages.basename=messages/messages
 로 설정을 하고, src/main/resources/messages/messages_en.properties..식으로 언어별로 만듬 , <span th:text="#{label.mostAwesomeWebsite}"></span>
or String msg = messageSource.getMessage("message.mostAwesomeWebsite",
http://g00glen00b.be/spring-internationalization-i18n/,,application.properties에 설정 대신 직접 messageSource 설정은 http://stackoverflow.com/questions/36531131/i18n-in-spring-boot-thymeleaf
     - angular : angular-translate 라이브러리는 너무 무거워 보이니, angular.filter 기능으로 값을 치환해 넣는 방식을 추천함.
     <h1><span ng-bind="(input | filter)"></span> more stuff</h1> = ng-bind="input | filter",   app.filter('name',[..function(input,options){ var output return output;
     https://scotch.io/tutorials/building-custom-angularjs-filters, http://blog.trifork.com/2014/04/10/internationalization-with-angularjs/

{{"views.main.Splendid!" | translate}}식으로 쓰면 안됨. <div proprerties안에 명식되어져야 함 ....
app/resources/locale-{{locale}}.json


//constant 대신에 value로 값을 주어서 값이 변할수 있게 하자
.constant('LOCALES', {
    'locales': {
        'ru_RU': 'Русский',
        'en_US': 'English'
    },
    'preferredLocale': 'en_US'
})

angular.module('translateApp', [
 ...
 'pascalprecht.translate',// angular-translate
 'tmh.dynamicLocale'// angular-dynamic-locale
])


.config(function ($translateProvider) {
    $translateProvider.useMissingTranslationHandlerLog();
})

.config(function ($translateProvider) {
    $translateProvider.useStaticFilesLoader({
        prefix: 'resources/locale-',// path to translations files
        suffix: '.json'// suffix, currently- extension of the translations
    });
    $translateProvider.preferredLanguage('en_US');// is applied on first load
    $translateProvider.useLocalStorage();// saves selected language to localStorage
})
