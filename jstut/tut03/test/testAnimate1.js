var myanimation = require('../module/animate1');

//jasmine testANimate1.js
//old way with browser: https://code.tutsplus.com/tutorials/testing-your-javascript-with-jasmine--net-21229

describe("A suite", function() {  //xdescribe(pass), xit

  var foo;
  spyOn(foo, 'setBar');

  beforeEach(function() {
    this.foo = 0;
    whatAmI = jasmine.createSpy('whatAmI');
    foo = {getBar:function(){return 1;}};

  });

  it("contains spec with an expectation", function() {
    this.bar = "test pollution?";
    spyOn(foo, "getBar").and.returnValue(745);
    //expect(foo.getBar).toHaveBeenCalled();
    //expect(foo.setBar).toHaveBeenCalledWith(456, 'another param'); //check arguments
    //spyOn(foo, "getBar").and.callFake(function(arguments, can, be, received) {
    //  return 1001;
    //});
    //spyOn(foo, "setBar").and.throwError("quux");
    //expect(foo.setBar.calls.count()).toEqual(2);
    expect(this.foo).toEqual(0);
    //expect(true).toBe(true);
  });
});
