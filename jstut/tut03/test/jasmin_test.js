//npm install jasmine-node --save


/* ../module/DataService.js
function DataService($http){}.....
module.exports = DataService;


또한 서버를 동작하게 하고 시피면 node server.js
https://ourcodeworld.com/articles/read/261/how-to-create-an-http-server-with-express-in-node-js

var express = require('express');
var app = express();
var exports = module.exports = {};

app.get('/', function(req, res){  //app.post("..., app.get('/the*man'...app.get(/^(.+)$/,
  res.send('Hello World');  //res.sendfile('index.htm'); , res.status(404).send("Sorry,...
});

var server = app.listen(3000, function(){
  console.log('Listening on on port 3000');
});

or in python : python -m http.server 8000 --bind 127.0.0.1 .... or flask
or in php : php -S localhost:8000 -t foo/
*/

//var request = require("request");  //npm install request --save

var dataService = require('../module/DataService');

describe("multiplication", function () {
  it("should multiply 2 and 3", function () {
    var product = calculator.multiply(2, 3);
    expect(product).toBe(6);
  });
});

//promise testing
describe("promise test", function () {
  it("should multiply 2 and 3", function (done) {
    dataService.getContents(id).then(function(response){
      expect(response).toBe(6);
      done();
    });
  });

  beforeEach(function() {
		var fetchPromise = new Promise(function(resolve, reject) {
			promiseHelper = {
				resolve: resolve,
				reject: reject
			};
		});
		spyOn(window, 'fetch').and.returnValue(fetchPromise);
		temperaturePromise = WeatherService.fetchCurrentTemperature();
	});

	it('fetches from the weather API', function() {
		expect(window.fetch).toHaveBeenCalledWith('someweatherapi.com');
	});

});
