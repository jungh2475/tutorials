How to record a video with audio in the browser with JavaScript (WebRTC)
https://ourcodeworld.com/articles/read/671/how-to-record-a-video-with-audio-in-the-browser-with-javascript-webrtc


video.js record plugin
https://github.com/collab-project/videojs-record


<link href="video-js.min.css" rel="stylesheet">
<script src="video.min.js"></script>
<script src="RecordRTC.js"></script>
<script src="videojs.record.js"></script>
<link href="videojs.record.css" rel="stylesheet">

<video id="myVideo" playsinline class="video-js vjs-default-skin"></video>

audio only
<script src="wavesurfer.min.js"></script>
<script src="wavesurfer.microphone.min.js"></script>
<script src="videojs.wavesurfer.js"></script>

<audio id="myAudio" class="video-js vjs-default-skin"></audio>



https://mozdevs.github.io/MediaRecorder-examples/index.html

navigator.mediaDevices.getUserMedia({
    audio: true
}).then(function (stream) {
recordButton.disabled = false;
recordButton.addEventListener('click', startRecording);
stopButton.addEventListener('click', stopRecording);
recorder = new MediaRecorder(stream);

// listen to dataavailable, which gets triggered whenever we have
// an audio blob available
recorder.addEventListener('dataavailable', onRecordingReady);
}
var recorder = new MediaRecorder(stream);
recorder.addEventListener('dataavailable', function(e) {
    // e.data contains the audio data! let's associate it to an <audio> element
    var el = document.querySelector('audio');
    el.src = URL.createObjectURL(e.data);
});

// start recording here...
recorder.start();

// and eventually call this to stop the recording, perhaps on the press of a button
recorder.stop();
