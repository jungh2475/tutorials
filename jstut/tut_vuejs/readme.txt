-----------------------------
install, 설치 ; https://medium.com/codingthesmartway-com-blog/vue-js-2-quickstart-tutorial-2017-246195cfbdd2
https://scotch.io/tutorials/build-a-to-do-app-with-vue-js-2

npm install -g vue-cli
vue init webpack vueapp01
vue init browserify-simple vueapp01
cd ...
npm install
//gitignore
npm run dev or npm run build (production)

//1.index.html

//2.src/App.vue
    <template>: <div id="app"><hello></hello>
    <script>: import Hello from './components/Hello',export default { name: 'app',  components: {  Hello  }}
    <style> #app

//3.src/main.js in folder src
//vue 2

import Vue from 'vue'
import App from './App'

new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})

---------------------------

//vuejs 1.x
<script src="http://cdn.jsdelivr.net/vue/1.0.16/vue.js"></script>
<script>
var vm = new Vue({
    el: '#vue-instance',
    data: {
      greeting: 'Hello VueJs!',
      inventory: [
      {name: 'MacBook Air', price: 1000},
      {name: 'MacBook Pro', price: 1800},
      {name: 'Lenovo W530', price: 1400},
      {name: 'Acer Aspire One', price: 300}
      ]
    },
    methods: {
      sayHello: function(){
        alert('Hey there, ' + this.name);
      }
    },
    computed: {
      doubleX: function() { return this.x * 2; }
    }
});
</script>

<div id="vue-instance">
    <span v-text="input_val"></span>
    <input type="text" v-model="greeting"><pre>{{ $data | json }}</pre>
    <button v-on:click="sayHello">Hey there!</button>  //=@click="sayHello"
    <ul><li v-for="item in inventory">
      {{ item.name }} - ${{ item.price }}
    </li></ul>
    <input type="number" v-model="x"> result: {{ doubleX }}
</div>


---------------------------------



<div id="app-2">
  <span v-bind:title="message">, title,class, text
    Hover your mouse over me for a few seconds
    to see my dynamically bound title!
  </span>
</div>


v-on:click="reverseMessage"

// Define a new component called todo-item
Vue.component('todo-item', { props: ['todo'],  template: '<li>{{ todo.text }}</li>'})

<ol>
  <!-- Create an instance of the todo-item component -->
  <todo-item></todo-item>
</ol>


<todo-item v-for="item in groceryList" v-bind:todo="item" v-bind:key="item.id"> </todo-item>
