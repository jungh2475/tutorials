어떻게  angular project를 만드는가?


/module/아래에 다음의 파일들을 만든다
  - app.js (모든 파일들을 합치는 기능, 각각은 module.export...require로 연결된다)
  - testapp.js (jasmine)  node 상에서 실험해 보려는 파일 .....node testapp.js 식으로 실행, 혹은 jasmine ...
  - services.js, controllers.js .......

/css, /js에는 compile되어져 합쳐진 최종 파일들이 들어 가게 된다
  - lib.js, lib.css는 gulp scripts(gulpfile.js) 실행해서 만듬(curl/wget으로 down)
  - 개발하면서 만드는 [browserify >] app.js->bundle.js, [node-scss -O ]style.scss->style.css

/html에는 templating html views들 저장해 둔다
  - index.html
  - listview.html ....등등


결국 spring에서 파일을 가져가는 것은 /html, /css, /js /img 일뿐이고 /module은 숨겨 지게 된다.

Controller 디자인에 관하여
  - subView를 html로 만들고, subVieController를 만든다 (여기에는 $rootScope.$on, logging Service, dataService등이 꼭 주입되어야 한다)
  - dataService는 - localAdapter안에 고정데이터를 주입하여(app.constant/value=>...) viewController를 browser에서 테스트 한다
  - nested controllers 가능하고, 각각은 $watch, $rootScope로 model 변화에 대한 view 업데이트를 할수 있다
  - model change에대하  view change를 잘 모르겠으면 다시 그려서 $scope 데이터들을 만들어 기존의 것과 차를 구해서 그것만 scope.apply한다


DataService 디자인에 관하여
  - localAdapter는 1차 개발용 with controller
  - httpAdapter(service)를 써서 ajax 연결을 해야하기에 promise로 데이터를 주로 가져온다 (확장하는 부분은 약간 자유가 있음)
  - jasmine으로 httpAdapter test 한다. 서버와 직접 연결하여 데이터 무결정성 테스트 함.
  - localStoagre/sessionStorage/chrome.storage.sync등을 사용할때 ...... 
  - syncList에 동기화 목록을 추가하고 perform_sync 할때........
