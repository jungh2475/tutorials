//install npm install -g browserify,  npm install {packages}
//사용례 browserify app-sample.js > ../../js/bundle.js, browserify app-sample.js -O ../../js/bundle.js

var mainController=require('./mainController');

var app=angular.module('app', []);
app.controller('MainController', mainController); //이거 동작 안함 app.controller('MainController', [$scope, mainController]);
