function myCollectionCtrl3($scope, dataService, $rootScope, logService){
  /* 준비해야 하는 scope 변수들은? : /html/crx/myCollections3.html참조
  */
  $scope.path;
  $scope.collections;
  $scope.comments;
  $scope.init=function(collection_id){
    buildViewData(collection_id,'on');
  };
  //$scope.init=function(collection_id){};

  //new, update, delete - comment, collection, keyword : 총 9가지 경우 (여기에 old-new data를 넣을 까?)
  $rootScope.$on('', function(event,data){
    buildViewData('off');
  });
  //$rootScope.$on('', buildViewData);

  function init(){
    buildViewData('on'); //dataService calling sync with server at first draw
  }

  function buildViewData(collection_id, dsync){
    var result={};
    var p1=dataService.getCollectionTree(collection_id,3); //depth level
    //var p0_1=dataService.getChildrenCollectionsByCollection(collection_id);  //business
    //var p1_1=dataService.getInventoryByInventory();  //journal
    //var p1_2=dataService.getCommentsByInventory();

    
    var p2=function(collection_tree_3){  // = buildCommentTree from collection_tree_3?
      return new Promise(function(resolve,reject){
        //
        resolve(data);
      });
    }

    funcion appendComments(collections){  //append comments...  collection_tree_3=[col1,col2,col3];
      var pArray=[];
      for(collection in collection_tree_3.collections){
        pArray.add(dataService.getCommentsByInventory(collection));
      }
      return Promise.all(pArray);
    }

    var p3=dataService.getCommentsByCollection(collection_id);

    var p1_all=p1.then(p2).catch(function(reason){});


    Promise.all([p1_all,p3]).then(function(values){  //Promise.all([p1,p2]).then(function(values){
      result.path=values[0];
      result.collections=values[1];
      result.comments=values[2];
      applyView(result);
    }).catch(function(reason){
      //예외처리..에러보고
    });

  }

  function applyView(data){
    $scope.$applyAsync(function(){
      $scope.path=data.path;
      $scope.collections=data.collections;
      $scope.comments==data.comments;
    });
  }
}



module.exports = myCollectionCtrl3;
