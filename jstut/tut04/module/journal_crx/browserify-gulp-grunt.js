
//grunt


//import the necessary gulp plugins
//npm install --save-dev gulp, npm install gulp-sass --save-dev
//gulpfile.js
var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');

//declare the task
gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});


//browserify



//sass/scss compile-css
//sass style.scss or node-sass my-sass-folder/ -o my-styles.css or node-sass -w sass/ -o css/
npm install node-sass //node->npm->node-sass
//package.json file (npm init will create one)
{ "scripts":{
  "test": "echo \"Error: no test specified\" && exit 1",
  "scss": "node-sass --watch scss -o css"},
}
npm run scss
