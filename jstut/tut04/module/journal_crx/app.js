/*

모든 파일들을 합치는 법
1)linux script: cat *.js > main.js or  https://github.com/dfsq/compressJS.sh ./compressJS.sh some-script.js another-sctipt.js onemore.js
2)grunt or gulp
3)browserify
4)webpack
*/
'use strict';

var angular = require('angular');  //angularjs+jsdom

var dataService=require('./dataService');
var sideViewController=require('./sideViewController');
var sumViewController=require('./sumViewController');
var dirViewController=require('./dirViewController');

var app=angular.module('app', []);
//app.config
app.service('dataService',dataService);
//app.controller('sideViewController',sideViewController); 이것만 동작하는듯, 아래 방식은 안됨.
app.controller('sideViewController',[$scope, dataService, sideViewController]);//현재 실험용
//app.controller('sideViewController',[$scope, $document, dataService, Logger, Subs, sideViewController]);
