var localAdpater = require('./localAdapter');
//some Adapters -

function dataService($http,$rootScope,logService,config){
/*
dataService의 역할은 adapter를 config에 의해서 초기화해서 construct해준다
http.promise return으로 인해, this.methods들은 대부분 promise return이 되어야 한다.

*/
  var adapter;
  var notify_hold; //on or off or on with timer

  //config값에 의해서 adapter 구성
  adapter=new localAdpater($rootScope,logService,conifg2);
  //serviceType = web or crx?, localStorage안에 storage형태를 정해 넣을까요?
  //chrome.sync.storage, chrome.local.storage, localStorage, sessionStorage, mem {}
  //or httpAdapter($http, httpService), or localHttpSyncAdapter(localApater-or-localStorage,httpService,SyncProcess)

  //this.methods for controllers
  var methods=['getCommentsByUrlAndUser','getInventoriesByUserIdAndType'];
  var mapped=[
    {d:'getCommentsByUrlAndUser', a:'getCommentsByUrlAndUser'},
    //{},
    //{}
  ];
  //adapter가 해당 method가 있는지 체크하는 루틴
  var keys=Object.keys(adapter);
  //if keys == methods? //array에서 만든 util에서 사용하세요.
  for(method in methods){
    //this[map.d]=adapter[map.a];
    this[method]=adapter[method];
  }

}


module.exports=dataService;
