//
/* storage(=repository)는 3종류가 있다(jsonMemory Object, browser storage, chrome storage)
각각에 변종이 있어서 총 5가지 정도이고, 그안에 memory jsonObject를 cache로 두면(속도향상?) 더 많아진다
{}(내부 memory json object), localStorage or sessionStorage, chrome.sync.storage, chrome.localStorage
그래서 이 녀석을 storageWrapper로 부르자. 이 녀석의 일은 k-v로 저장된 녀석을 mysql/table/springRepo형식으로 자료 입출력을 하게 해준다
그리고 node/jasmine test를 위해서 가짜 localStorage, chrome.sync.storage등을 만들어서 넣어 줄수가 있다
*/

function storageWrapper(key_storage){
/* key-value로 된 storage에서 데이터 입출력을 translate해주는 기능을 가짐
   사용법 key_storageWrapper.comments().then(function(response){.....});
   key에는 'comment_34'이런식이나 'comment_d34' (=draft34) syncAdapter인경우 생기는 문제임.
*/
  this.comments=function(){
    //comments모두를 return하는 함수
    //key_storage.
  };
  this.getComment=function(id){

  };
  this.setComment=function(comment,callback){
    var id=comment.id;
    var key='comment_'+id;
    key_storage.setItem(key,JSON.stringify(comment));
    //여기서 notify하면 레벨이 너무 내려가있는것이고, this를 사용하는 데서 notify하게 하자
    callback(comment);//or callback();
  };

  this.getCommentsByUrl=function(url){
    var key_contains='comment_';
    //for-loop: key_storage.get(key_contains)
  };

}
//위의 것 대신해서 변화기만 장착해서 쓰는 방식이 있다
function keyStorageUtil(){

}

//adapter들은 find/get을 할때는 그냥 자료를 주면되고, set을 해서 데이터가 바뀌면 notify를 controller들에 해주어야 한다
function localAdapter(storage, notify){

  this.getCommentById=function(id){
    return new Promise(function(resolve,reject){
      //storage.comment(id).then(function(response){resolve(response);}).catch(...);
    });
  };
  this.getCommentsByUrl=function(url){
    return new Promise(function(resolve,reject){
      //
    });
  };

  this.setComment=function(comment){
    return new Promise(function(resolve,reject){
      //
      storage.setComment(comment,notify(comment)); //저장되면 callback으로 부른다
      //notify(comment); //or notify(); notify('comment',comment);
    });
  };

}

function syncAdapter(storage, httpService, notify){

  this.getCommentById=function(id){
    return new Promise();
  };

}

function dataService($http, $rootScope, config){
/*
  여기서는 세팅 및 서브들을 구성/장착한다. 메소드들이 각 adapter에 잘 준비 되어져 있는지도 확인하고,
  서비스를 구성하는 매니져(관리자) 역할임
  background.js는 angular일 필요는 없다.
*/
   var adapter;
   function init(){
     //according to config

     var storage =   //

     adapter=new localAdapter(storage, notify);
     //
     var httpService = new httpService(config.host, config.user, config.password);
     httpService.connect();
     //if everything is ok, then...
     adapter=new syncAdapter(storage, httpService, notify);
   };

   function notify(change){

   };

}
