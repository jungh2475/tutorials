

function SyncStorageAdapter(storage, httpService, syncer, notify, logger){

  //밖(dataService)에서 syncer를 만들어 가지고 와서 주입한다

  this.getCommentsByUrl=function(url){
    return new Promise(function(resolve,reject){
      storage.getCommentsByUrl(url).then(function(comments){
        resolve(comments);
        syncer.add({'method':'getCommentsByUrl', 'param':url });
      }).catch(function(error){});
    });
  };

  this.setComment=function(comment){
    return new Promise(function(resolve,reject){
      storage.setComment(comment).then(function(comment){
        syncer.add({'method':'setComment','param':comment});
        notify('comment', comment);
        resolve("ok");

      }).catch(function(reason){console.log("localStorage save error!"); logger.log(reason);});
      /*
      아래처럼 하면 안됨.
      Promise.all([storage.setComment(comment),httpService.setComment(comment)])
      .then(function(){
        console.log('done well');
        resolve(comment);
        notify('comment', comment);
      }).catch();
      */
    });
  };
  this.removeComment=function(id){
    //this.name, id
    return new Promise(function(resolve,reject){
      storage.removeComment(id).then(function(id){
        syncer.add({'method':'removeComment','param':id});
        resolve("ok");
        notify('comment_del', id);
      }).catch(function(reason){console.log("localStorage remove error!"); logger.log(reason);});

    });
  };

  this.xxx=function(y){
    var that=this;
    InnerWork(that.name, y);
  };

  function InnerWork(name, param){
    return new Promise(function(resolve,reject){
      storage[name](param).then(function(response){
        syncer.add({'method':name, 'param':param});
        resolve(response);
        if(name.indexOf('get')==-1){ notify();}
      }).catch();
    });
  }

}

module.exports=SyncStorageAdapter;
