/*

$scope를 통째로 넣어서 테스트도 간으하나, 그러면 1차 레벨의 것들중에 다른 것만 판단하게 되서 많이 다시 $scope.$apply를 적용해야 할것같아. 한단계아래인
$scope.commentViews 같은 array에서의 모델의 add/update/delete를 추적해서 변경이 감지된 array의 index들을 찾도록 합니다.
var indexs=Object.keys(diffUtil.diff($scope.commentViews, newCommentViews));
이를 기준으로  $scope.commentViews 안의  해당 index들을 바꾸어 줍니다.
문제는 삭제되는 것은 undefined로 남게 되는데,...이를 일일히 splice()로 제거하던가 아니면 filter로 제거하고 $scope.commentViews 전체로 apply하냐입니다.
결국은 전체로 apply할까요?

*/
//about diff functions : https://www.npmjs.com/package/deep-object-diff
//npm i --save deep-object-diff

var diffUtil = require("deep-object-diff");

var objA = {
  'a':[{'a':'b','c':'d'},{'a':'x','c':'y'},{'a':'b1','c':'d1'},{'a':'b','c':'d'}],
  'b':'ccc',
  'd':{'a':'b', 'c':'d', 'e':[{'a':'x1','c':'y1'},{'a':'b','c':'d'}]}
};
var objB = { //같은 크기에 내용만 바뀐 경우
  'a':[{'a':'s','c':'t'},{'a':'x2','c':'y'},{'a':'b1','c':'d1'}],
  'b':'vv',
  'd':{'a':'w', 'c':'d', 'e':[{'a':'x1','c':'y1'},{'a':'y','c':'d'}]}
};

var objC = { //항목만 같고, array  크기도 다르며 내용도 바뀐 경우
  'a':[{'a':'bd','c':'dk'},{'a':'px2','c':'ky2'},{'a':'b3','c':'d3'},{'a':'px','c':'y'},{'a':'pb1','c':'d1'}],
  'b':'cffc',
  'd':{'a':'pb', 'c':'pd', 'e':[{'a':'r','c':'yg'},{'a':'b1','c':'d1'},{'a':'s1','c':'w1'},{'a':'d','c':'e'}]}
};


console.log("we did run!!");
/*
  만들어 볼 $scope 데이터들
  (crx)
  $scope1.commentViews
  $scope2.collectionViews (directoryView)
  $scope3.keywordsViews
  $scope4.sharedCollectionViews
  $scope5.groups
  $scope6.settings

  (web diary)
  $sopce7.dayView
  $scope8.timelineView
  $sopce9.chapterView
  $sopce10.collectionViews

*/


var myArray =[];
//isArray, typeof object ==
myArray.filter(function(item){});

/*사용 목적 :
  var newScope = buildViewData();
  var results = diff(newScope, $scope);
  $scope.asyncApply(function(){
    results.forEach(function(item){
      $scope[item.scopeName]=item.scopeData;
    });
  });


*/
//1)만들어진 객체가 object 형태인 경우  var scope_obj=diff(new_scope, $scope);
function scopeApplier(scope_obj){
  $scope.$applyAsync(function(){
    for (var p in scope_obj){  //key in obj
      if( scope_obj.hasOwnProperty(p) ) {
        $scope[p]=scope_obj[p];
      }
    }
  });

}

//1)만들어진 객체가 array_object 형태인 경우  var scope_array=diff2(new_commentViews,$scope.commentViews);
function scopeApplier2(scope_name, scope_array){
  $scope.$applyAsync(function(){
    $scope[scope_name]=scope_array;
  });
}

function scopeApplier3(scope_name, is, scope_array){
  $scope.$applyAsync(function(){
    is.forEach(function(item){});
  });
}
function diff(objA, objB){  //

  var result={};
  data={};
  data.scopeName="";
  data.scopeData={};
  result.push(data);
  return result;
}

function diff2(arrayA, arrayB){
  var result=[];
  //구현하자
  return result;
}

console.log("compare objs");
var result=diffUtil.diff(objA, objB);
console.log(Object.keys(result));  //Object.values, Object.entries
console.log(result);

console.log("compare arrays");
var result=diffUtil.diff(objA.a, objB.a);
console.log(Object.keys(result));
console.log(result);

var url="http://abc.com";
//var newCommentViews=buildCommentsViewData(url);
//테스트용 데이터를 주입합니다.
newCommentViews=objB.a;
var $scope={};
$scope.commentViews=objA.a;
console.log("is its array?:"+Array.isArray(newCommentViews));
var result = diffUtil.diff($scope.commentViews, newCommentViews);
var arrayItems = Object.keys(result);
var scope_array =[];
arrayItems.forEach(function(item){
  scope_array.push(newCommentViews[item]);
});

console.log("inputs for scopeApplier3 is");
console.log(arrayItems);
console.log(scope_array);
//scopeApplier3('commentViews',arrayItem,scope_array)
//삭제가 된 elm은 $scope.commentViews에서 제거해야 합니다.
//$scope.commentViews.filter(function( element ) { return element !== undefined;});
//$scope.bdays.splice(index, 1);
//만약에 하나라도 다르면 전체를 다 그리던가, 해당 array[i] 녀석들만 바꾸어서 적용합니다.
