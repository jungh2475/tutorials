function Syncer(storage, httpService, notify){
  var synList=[];
  /*
    내가 set/remove하는 행동들은 create외에는 sync할 필요가 없다. set/remove를 실패하였을 경우에는? 연결실패이면 그냥 두고, 거부이면 알린다(계정 다시 로그인? 그리고 완전 실패일떄 값들을 돌릴수 있는가?
    array [
        {method:'getCommentsByUrl', param:'abc.com'},
        {method:'setComment', param:{id:-, text:'hello', time:123}},
        {method:'setComment', param:{id:2, text:'hello-updated', time:123}},
        {method:'setComments', param:[{id:2, text:'hello-updated', time:123},{id:3, text:'hello-3', time:1223}]},
        {method:'removeComment', param:3},
      ]
  */

  //var syncParams
  var syncInterval=2000; //msec
  var syncIntervalMax=100000; //100초
  var syncIntervalMax=1000;
  var lastSyncedTime = 0;
  var performSync_que = false;

  //localStorage and server model lastUpdatedTimeStamp
  var modlesLastUpdatedTimeStamp ={
    user:12312,
    content:912343,
    comment:44444,
    inventory:343423
  };

  this.add=function(item){
    //동일한 synItem이 있으면 bypass함.
    var currenttime=(new Date).getTime();
    if(currenttime-lastSyncedTime>syncInterval){

      performSync();
    }
    else{
      //이미 performSync가 예정이 되어져 있다면 그냥 나가면 되고, 아직 세팅이 안되어져 있다면, 이를 설정해 준다
      if(!performSync_que){
        setTimeout(this.add,(syncInterval-(currenttime-lastSyncedTime)));  //nextSyncTimeOffset
        performSync_que = true;
      }
    }
    //performSync();
  };

  this.performSync=function(){
    //시간이 되었으면 sync를 실행, 아니면  setTimeout로 나중에 실행하도록 조정한다?
    //하나씩 꺼내던가 promise all로 다 동작 시켜봄
    Promise.all(promises).then(compare);
    lastSyncedTime=(new Date).getTime();  //var d1 = new Date(); d1.toUTCString();, Math.floor(d1.getTime()/ 1000)
    performSync_que = false;
    //check dataChanges in Server
  };

  function initData(){

  };

  function check_timestamp(){
    //httpService.getTimeStamp('comments', userid)
    httpService.getTimeStamp(userid).then(function(results){
      if(diff(results,modlesLastUpdatedTimeStamp)) {
        //if(){ initData();}
        initData();  //then notify();
      }
    });

  }

  function diff(A, B){  //returns true(different) or false(actually same)
    if( Atype != B type) { return 'error';}
    if(A.isArray()){ diffArrById(A,B);}
    else if (A is Object) {
      diffObj(A,B)
    } else{
      //somethign wrong@!
    }
  };

  function diffObj(objA, objB){
    //see diff_test.js,, 두개 객체면 member_properties들안의 값이 혹은 array이면 순서에 상관없이 element_item안의 객체들을 비교해서(e.g. id)
    //Object.is(objA, objB) Object.keys(objA), Object.values(objA),
    var result=true;
    var arr_keys=Object.keys(objA)
    arr_keys.forEach(function(key){
      var bValue=objB[key];
      var aValue=objA[key];
      if(aValue!=bValue){ result=false;}
    });
    //for(key in Object.keys(objA)){ //이건 0,1,2...index가 return이 됨,.이상함   }
    return result;
  };

  function diffArrById(arrA, arrB){ //각 array의 구성요소들안의 id가 같은 것 끼리 비교해서 같은  array인지 비교한다
    var result=true;
    for(objA in arrA){
      var temp_result=true;
      //by id or else?
      var pos=arrB.findIndexOf(function(element){return element.id==objA.id;});
      if(pos[0]){
        var objB=arrB[pos[0]];
        temp_result=diffObj(objA,objB);
        if(temp_result==false) {result=false;}
      }

    }
    return result;
  };

  function compare(responses){  //[p1, p2, p3.....]
    var i=0;
    for(response in responses){
      var i = responses.indexOf(response);
      var method = synList[i].method; //'getCommentsByUrl'; //get from promise item
      var param = synList[i].param;
      //if(storage[method](param)!= response){ //내용전체를 비교하던가, 내용 항목들이 달라졌는지만 확인?
      if(diff(storage[method](param),response)){
        //storage.set....여기가 중요...이 결과를 어떻게 저장하는가?
        storageSet(synList[i],response).then(function(response){
          //if response is ok
          notify();
        });
        updateInterval('reduce');
      }
    }
  };

  function updateInterval(success){
    //비교해서 데이터간에 차이가 없어서 sync 의미가 없어진 경우, synInterval을 늘린다.  성공했으면 interval을 줄인다
    var factor={success:0.5, fail:2};
    if(success='increase'){
      if(syncInterval>syncIntervalMax){
        syncInterval=syncInterval*(factor.fail);
        if(syncInterval<syncIntervalMin){syncInterval=syncIntervalMin;}
      }
    } else{ //='reduce', no change, last syn was meaningless
      if(syncInterval<syncIntervalMax){
        syncInterval=syncInterval*(factor.success);
        if(syncInterval>syncIntervalMax){syncInterval=syncIntervalMax;}
      }
    }

  };


  function storageSet(syncListItem, response){
    return new Promise(function(resolve, reject){
      // 상황에 따른 다 다른 접근법?



    });
  };

}
