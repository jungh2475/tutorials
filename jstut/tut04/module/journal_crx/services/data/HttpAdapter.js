function HttpAdapter(httpService, notify, logger){

  this.getCommentsByUrl=function(url){
    return new Promise(function(resolve,reject){
      httpService.getCommentsByUrl(url).then(function(comments){
        resolve(comments);
      }).catch(function(error){});
    });
  };

  //set, remove 동작을 할때는 notify가 날라가야 한다.
  this.setComment=function(comment){
    return new Promise(function(resolve,reject){
      httpService.setComment(comment).then(
        resolve("ok");
        notify('comment', comment);
      ).catch();

    });
  };

  //...........

}

module.exports=HttpAdapter;
