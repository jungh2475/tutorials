

var objA = {
  'a':[{'id':'1','c':'d'},{'id':'2','c':'y'},{'id':'3','c':'y'},{'id':'4','c':'d'}],
  'b':'ccc',
  'd':{'id':'b', 'c':'d', 'e':[{'a':'x1','c':'y1'},{'a':'b','c':'d'}]}
};
var objB = { //같은 크기에 내용만 바뀐 경우
  'a':[{'id':'1','c':'t'},{'id':'3','c':'y'},{'id':'4','x':'x'}],
  'b':'vv',
  'd':{'a':'w', 'c':'d', 'e':[{'a':'x1','c':'y1'},{'a':'y','c':'d'}]}
};



var diffUtil = require("deep-object-diff");
//diff, addedDiff, deletedDiff, updatedDiff, detailedDiff.
//..이것을 쓰려면 먼져 array.sort(function(a,b){return a.id-b.id})  or char.toUpperCase()...if (nameA < nameB)

var id_value=2
function findElement(element){
  return element.id==id_value;
}

//array에서 해당 id를 가진 녀석이 어디 있는지 찾을 때
function findId(value){  //someArray.findIndex(findId(4))
  return function (element){return element.id==value;};
}

/* 서버에서 getCommentsByUrlAndUser로 comments를 가지고 와서(혹은 id들만),
내 localStorage(cached)된 녀석과 동기화 할때, 아래의 util function들을 쓴다

if timestamp different.....(lastUpdatedTime-by-user/을 항상 snapshot에서 찾을수 있나?)
var result=diff(localStorage.comments.map(getProperty('id')),
                server.getCommentsByUrlAndUser(url,user).map(getProperty('id')));

*/

//array에서 elm 객체의 요소만 추출하고 싶을때
function getProperty(name){  //someArray.map(getProperty('id'))
  return function(elm){return elm[name];};
} /* or function getIds(someArray){return someArray.map(function(elm){return elm.id;})} */


//두개의 array에서 교집합,차,여를 구하고 싶을 때
function diff(arrA, arrB){  //diff([1,3,4],[3,4,2])=> 공통(3,4), removed:-(1), toAdd:+(2)
  var result={};
  //isArray?
  //arrB.forEach(function(elm){var i=arrA.indexOf(elm); if(i>-1){}});
  result.common=[];
  result.remain=JSON.parse(JSON.stringify(arrA)); //copy되게 해야지 않그러면 원본이 바뀐다.
  result.toadd=[];
  var i;
  arrB.forEach(function(elm){
    i=result.remain.indexOf(elm);
    if(i==-1){result.toadd.push(elm);}
    else{result.common.push(elm); result.remain.splice(i,1);
    }
  });
  return result;
}

//중복된 element 제거: arr=>id만추출 =>중복id제거 =>해당 id로 다시 arrFiltered만들기 arrIndex.forEach(function(index){result.push(arr[index]);})
arr.map(getProperty('id')).filter( function( item, index, inputArray ) {
           return inputArray.indexOf(item) == index;
    });

//Array.findIndex(findElement);
//Array.findIndex(findId(2));
//objA.a.includes();
