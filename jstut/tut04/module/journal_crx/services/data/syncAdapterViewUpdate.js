

//ES6에서 내가 속한 function의 이름을 얻는 법: myFunction.name

function lg(tag,msg){
  console.log(""+tag+"::"+msg);
}



function SyncAdapter(localStorage, httpAdapter, $rootScope){

  var localStorage=localStorage;
  var httpAdapter=httpAdapter;
  var syncList=[];
  var $rootScope=$rootScope;

  this.getCommentsByUrl=function(url){

    //localStorage.
    return new Promise(function(resolve,reject){
      if(url){
        resolve("received_url:"+url);
      } else {
        reject("error"); }
    });

  };

};

function dataService($http, $rootScope, appData){

    var adapter=new SyncAdapter();
    this.getCommentsByUrl=adapter.getCommentsByUrl;



}

function buildViews(seedData){

  this.buildSideNote=function(data){};
    this.buildView_AboutPage=function(data){};
    this.buildView_Comments=function(data){
      lg("!","buildView_comments");
      var result=[];
      //for(comment in data){ }
      result="<div>hello world</div>";
      return result;
    };
    this.buildView_AlsoExtra=function(data){};

  this.buildSummaryPage=function(data){};
    this.buildView_Collection=function(data){};
      this.buildView_CollectionDirectory=function(data){};
    this.buildView_SharedWithMe=function(data){};
    this.buildView_MyGroups=function(data){};
    this.buildView_SettingsView=function(data){};

}



function ViewController($scope, dataService, $rootScope){

  var buildViews = new buildViews();
  var ViewObservers=[];

  var mapped={'responseObject':'comments','data_requester':'getCommentsByUrl','data_arguments':'abc','scope_target':'comments','buildView':'buildView_Comments'};

  function buildView(mapped){
    //dataService=>promise.all로 한꺼번에  dataService에 물어봐서 responses를 가져오게 할 수도 있다
    dataService[mapped['data_requester']](mapped['data_arguments']).then(function(response){
      $scope.$applyAsync(function(){
        $scope[mapped['scope_target']] = buildViews[mapped['buildView']](response);
      });
    }).catch(function(error){console.log("promise reject error ...");});
  };

  //1)make SideNotePage
  buildView(mapped);

  //2)make SummaryPage


  }
  //from $broadcast(msg,data)
  /*
  $rootScope.$on(msg, function(msg){

    if(ViewObServers[msg]!=null){
        //ViewObServers[msg](data);
    }

  });
  */


};




lg("**","app...started");
//var app={};
//app.config
//app.service
//app.controller('')
var dataService = new dataService();
var $scope={'comments':"sd"};
console.log("dfsdfsdf");
console.log("$scope.comments:1:"+$scope.comments);
console.log("$scope.comments:2:"+$scope['comments']);
var $rootScope={};
var viewController= new ViewController($scope, dataService);
lg("**","app...end executed");
