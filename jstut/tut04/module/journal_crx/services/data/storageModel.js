
function chromeStorageModel(mode){  //chrome.storage.sync 혹은 localdp 쓰일수 있음
  var inner_storage={};
  var mode=mode;
  var max_count=1024;

  this[mode].get=function(keys,callback){
    //null, string, object(specifying default values in dic: {'category':'fruit'}) or array of strings
    var value=inner_storage[key];
    callback(value);
  };
  this[mode].set=function(k_Obj,callback){  //object items
    //is typeof k_obj = 'str', obj?
    //if exist then update, not exist then create
    callback();
  };
  /* .set({kitten:  {name:"Mog", eats:"mice"}, monster: {name:"Kraken", eats:"people"}},callback);
  */

  this[mode].remove=function(keys,callback){
    callback();
  };

  this.onChanged.addListener = function(callback){  //changes, namespace
    //
    callback(changes,namespace);
  };

  //chrome.runtime.lastError
  //storage.managed storage is read-only, manifest: "permissions": ["storage"],

};

//아래 직접구현보다는 이 npm module을 쓰자 npm install node-localstorage https://www.npmjs.com/package/node-localstorage
function localStorageModel(){  //browser localStorage 혹은 sessionStorage에 쓰임

  //obj=JSON.parse(str), parseInt
  //str=JSON.stringify(obj)
  this.length=200;
  this.getItem=function(key){return value;};
  this.setItem=function(key,value){};
  this.removeItem=function(key){};
};

function testModel(){
  if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}
  //localStorageModelTest
  for(key in localStorage){
    var obj =JSON.parse(localStorage[key]);
    if(obj.member =='john') {return;}
  };
};
