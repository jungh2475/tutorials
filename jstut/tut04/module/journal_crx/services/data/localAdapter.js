function LocalAdapter(storage, notify, logger){
  //역할을 storageWrapper에 저장하고, 데이터모델이 변경되었을때 적절한 notify를 해준다

  var mode;

  this.init(mode){

  };

  this.getCommentsByUrl=function(url){
    return new Promise(function(resolve,reject){
      storage.getCommentsByUrl(url).then(function(comments){
        resolve(comments);
      }).catch(function(error){});
    });
  };

  //set, remove 동작을 할때는 notify가 날라가야 한다.
  this.setComment=function(comment){
    return new Promise(function(resolve,reject){
      storage.setComment(comment).then(
        resolve("ok");
        notify('comment', comment);
      ).catch();

    });
  };

  this.removeComment=funciton(id){
    return new Promise(function(resolve,reject){
      storage.removeComment(id).then(
        resolve("ok");
        notify('comment_del', id);
      );
    });
  };


}

module.exports=LocalAdapter;
