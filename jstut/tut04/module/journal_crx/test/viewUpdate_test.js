
console.log("starting.....");
var arrayA = [];
var arrayB = [];
var objA = { id:1, text:"hello world", time:"20170810", visib:"private", collections:"fruit", keywords:"apple, orange"};
var objB = { id:2, text:"hello steve", time:"20170910", visib:"private", collections:"fruit", keywords:"orange"};
var objC = { id:3, text:"hello john", time:"20171010", visib:"public", collections:"fruits", keywords:"lemon"};
var objD = { id:4, text:"hello kewtea", time:"20171110", visib:"public", collections:"cars", keywords:"alfa_romeo"};
var objE = { id:5, text:"hello journal", time:"20171210", visib:"group", collections:"cars", keywords:"tvr"};

console.log(objD);
console.log("starting functions.....");


function buildData(){
  arrayA=[objA, objB, objC, objD, objE];
  //arrayA.push(,,), push.apply

  arrayB=[objA, objD, objC, objB, objE];
}

buildData();
//promise Thenable
//var p1 = Promise.resolve or reject(value, promise or thenable)
var thenable ={ then:function(resolve){ resolve('Resolving');} };

//function dataService($http){}
function testController($scope){
  $scope.carname = "Volvo";
}



function viewController1($scope){

  function applier($scope_subName, data){
    $scope.$applyAsync(function(){
      $scope[$scope_subName] = data;
    });
  }

  function buildViewDataApply(){
    var data ={};
    /*
    dataService.getxxx.then(function(response){
        //
        //
        applier('comments',data);
    }).catch();

    */
  };

  $rootScope.$on('',function(){
    buildViewDataApply();
  });

  //$scope.vm.comments ....vm.xxx, vm.yyy....,.....
  //변수,clickAction들을 나열합니다......
  $scope.comments=arrayB;//[];//['a','v'];
  $scope.mode = 1;
  $scope.actionName ="";
  $scope.init = function(){
    buildViewDataApply();
    //var comments = arrayA; //[];
    //applier('comments',arrayA);
  };

  $scope.click=function(actionName){
    switch(actionName) {
      case 1:
        break;
      case 2:
        break;
      default:
        break;
    }

  };

  $scope.removeA = function(index){
    console.log("index:"+index);
  };
  $scope.removeB = function(id){
    console.log("id:"+id);
  };

  $scope.add = function (index){};
  $scope.remove1 = function(index){
    $scope.comments.splice(index,1);
  };

  $scope.remove1apply = function(index){
    $scope.comments.splice(index,1);
    applier('comments',$scope.comments);
  };

  $scope.remove1promise = function(index){
    //진짜로는 이런식으로 동작할것임. dataService에 가서 결과를 가지고 와서 저장/변경후에 데이터 조작....
    var p1=Promise.resolve(
      $scope.comments.splice(index,1)  //returns deleted items array
    );
    p1.then(function(value){applier('comments',$scope.comments);});
  };

  //새로나온 $scope.comments에서 변경된 데이터만 scope.apply할 방법은? -update일때만 변경하고, 삭제/추가인 경우에는 전체 적용해야...?

  $scope.remove1refresh = function(index){

  };

  $scope.toggle = function(){
    /*dataService.getData().then(function(result){
      //
    });
    */
    var p1=Promise.resolve();
    p1.then(function(){
      if($scope.mode==1) {$scope.mode=0;applier('comments',arrayA); console.log("arrayA");}
      else{ $scope.mode=1; applier('comments',arrayB); console.log("arrayB");}
    });


  }



}
/*
console.log("all functions loaded and init..controller");
var $scope={};
var dataService = new dataService();
var viewController = new viewController($scope, dataService);
*/
//change $scope.comments
