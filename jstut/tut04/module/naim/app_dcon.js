/*

sample for
  1. dynamic controllers (add/remove)
  2. overriding scene with spring socket conection
*/



//dataService.js
//mainController.js

//angular component를 넣는 경우
//subController1.js  - for gSheetReader
//subTemplate1.html


//html with <script> javascript

//화면의 ng-bind-html안에 sub_html+<script>를 넣는다


'use strict';
var angular = require('angular');  //angularjs+jsdom
var dataService=require('services/dataService');
var mainController=require('controllers/mainController');

var app = angular.module('naimApp',[]);  //["ngSanitize"] //angularjs+jsdom
app.service('dataService',dataService);
app.controller('mainController',mainController); //$scope, $sanitize? $sce?
