
/*
  <div ng-bind-html="div1_html_safe></div>
  <div ng-bind-html-unsafe="div1_html_unsafe"></div>
*/

function mainController($scope, $sce){

  $scope.div1_html_safe=$sce.trustAsHtml($scope.div1_html_unsafe);

}
module.exports = mainController;
