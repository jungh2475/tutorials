
/*
var https = require('https'); //https.get('',(resp)=>{resp.on('')}).on('error',(err)=>{});
var request = require('request'); //npm install request, request('',{json},(err, res, body) => {
  if (err) { return console.log(err); }...
  });
var axios = require('axios'); //axios.get('').then(response=>{}).catch(error=>)
axios.all([axios.get('....]).then(axios.spread((response1, response2) => {})).catch(error => {
*/


//https://mail.google.com/mail/u/0/#search/sangsoo.lee%40kewtea.com/15b9dd333ff8f21e
//for naim gSheetSuper theme : unibox, fb/insta,gDrive(gPhoto),dashBoard, gaLog,....
//https://docs.google.com/spreadsheets/d/1wL5sx6gVLZxyYQf5b2wYkJCcdIX14B0gWZ99l6f2Jr0/edit?usp=sharing
function gSheetReader(sheetId,sheetNum){
  var gSheetUrl = "https://spreadsheets.google.com/feeds/list/"+sheetId+"/"+sheetNum+"/public/values?alt=json";
  this.getJson=function(url){
    return new Promise(function(resolve, reject) {
      axios.get(url).then(response=>{
        try {
          var data = [];
          var rows =response.data.feed.entry;
          for (var i=0; i<rows.length; i++) {
                        data[i] = {};

                        var row = rows[i];
                        var rowNames = Object.getOwnPropertyNames(row);
                        var filtered = rowNames.filter(function(value) {
                            return value.indexOf("gsx$") > -1;
                        });

                        for (var j=0; j<filtered.length; j++) {
                            var column = filtered[j].substr(4);
                            data[i][column] = row[filtered[j]].$t;
                        }
                    }
          resolve(data);
        } catch(error){
          console.warn(error);
          //if (window.ga && ga.create) ga('send', 'exception', {'exDescription':"
          reject(error);
        }
      }).catch(error=>{});
    });

  }
}
