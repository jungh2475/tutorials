//install: brew install node, npm init, npm install gulp gulp-concat --save-dev
//npm link gulp gulp-concat
//usage: gulp scripts  for more: http://learningwithjb.com/posts/concat-combining-multiple-files-into-one-with-gulp

var gulp = require('gulp');
var concat = require('gulp-concat');


gulp.task('scripts', [], function() {

  gulp.src(['./angular.min.js', './angular-animate.min.js'])  //"contents/styles/**.css" or []
      .pipe(concat('lib.js'))
      .pipe(gulp.dest('.'));

});

//https://callmenick.com/dev/an-introduction-to-gulp/
