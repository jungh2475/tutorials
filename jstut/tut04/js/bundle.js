(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
//install npm install -g browserify,  npm install {packages}
//사용례 browserify app-sample.js > ../../js/bundle.js, browserify app-sample.js -O ../../js/bundle.js

var mainController=require('./mainController');

var app=angular.module('app', []);
app.controller('MainController', mainController); //이거 동작 안함 app.controller('MainController', [$scope, mainController]);

},{"./mainController":2}],2:[function(require,module,exports){


function mainController($scope){
  var vm = this;
  vm.title = 'controller title is here';
  console.log("main controller loaded");
  $scope.title="MainController_scope_title Here!!";
  $scope.init=function(){
    console.log("main controller init() loaded");
  };
}


module.exports = mainController;

},{}]},{},[1]);
