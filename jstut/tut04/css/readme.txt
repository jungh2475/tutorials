rm -rf node_modules
sudo rm -rf /usr/local/lib/node_modules
sudo rm -rf ~/.npm
brew uninstall --force node
or brew upgrade node
npm install npm@latest -g

-------------------
brew install node
npm -v  or node -v
npm install -g node-sass
which node-sass
node-sass sample.scss sample.css
or nodemon -e scss -x \”npm run build-css\”

-------------------
scss 문법
http://callmenick.com/post/an-introduction-to-sass-scss
https://scotch.io/tutorials/how-to-use-sass-mixins

@import 'reset';
$color-primary : rgb(40,40,40);

@mixin border-radius($radius) {
  -webkit-border-radius: $radius;
     -moz-border-radius: $radius;
      -ms-border-radius: $radius;
          border-radius: $radius;
}

.box {
  @include border-radius(10px);
}

@mixin pad ($pads...) {
  padding: $pads;
}

.aaa
  .xxx {
    @extend .col-2-3;
  }


$colors: (
  primary: #333,
  secondary: #555,
  brand: #ff0033
);
$box-style2: (bStyle: dotted, bColor: blue, bWidth: medium);


color: map-get($colors, secondary);
@each $key, $value in $colors {
  .item-#{$key} {
    color: $value;
    @debug ($key, $value);
  }
}

@mixin break($size) {
  @media (min-width: map-get($breakpoints, $size)) {
    @content;
  }
}
