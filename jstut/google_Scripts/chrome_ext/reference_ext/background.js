
//http://altitudelabs.com/blog/how-to-write-your-first-chrome-extension/
//https://developer.chrome.com/extensions/event_pages
//데이터를 외부 세계와 연결(전달/배달)해주는 dataService역할을 한다. 전체 리소스를 관리한다

/*

  don't use window.setTimeout() or window.setInterval(), switch to using the alarms API
  If your extension uses, extension.getBackgroundPage, switch to runtime.getBackgroundPage

*/


//chrome.browserAction.setPopup(object details);


// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function(tab) {
  // Send a message to the active tab
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    var activeTab = tabs[0];
    chrome.tabs.sendMessage(activeTab.id, {"message": "clicked_browser_action"});
  });
});
