import { JWT } from 'google-auth-library';

credentials = JSON.parse(
    await promisedFile(resolve(__dirname, '../credentials.json'), 'utf-8'),
  );
  const client = new JWT({
    email: credentials.client_email,
    key: credentials.private_key,
    scopes: ['https://www.googleapis.com/auth/spreadsheets'],
  });
  await client.authorize();


  import { google } from 'googleapis';
import { auth } from './auth';

const sheetsApi = google.sheets({ version: 'v4' });
