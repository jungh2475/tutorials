var MyController = require('./MyController');
var $scope = {};

//@mock
var dService = {
  getContentById : function(id){
    return new Promise(function(resolve,reject){
      resolve(id);
    });
  },
  xxx:"hihihhh"
};
var mController = new MyController($scope, dService);

function testMyController(){

  mController.method3(3);
  mController.method2(15);
  mController.method1(23);  //무조건 적은 값이 답으로 나오도록 만든 dService를 주입하였음
  //console.log("scope.result:"+mController.$scope.result);

}


testMyController();
