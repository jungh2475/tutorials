

//var DataService = require('DataService');
//var dService = new DataService();

function MyController($scope,DataService){

  //var

  var methodSuccess = function(response){
    console.log("response:"+response);
    $scope.result=response;
    console.log("scope.result:"+$scope.result);
    /*
    $scope.apply(function(){
      $scope.result=response;
      console.log("scope.apply.....result:"+$scope.result);
    });
    */
  }

  var methodError = function(response){
    console.log("error:"+response);
  };


  this.method1 = function(id){
    console.log("method1 called");
    DataService.getContentById(id).then(function(response){
      //console.log("response:"+response);
      $scope.result=response;
      console.log("scope.result:"+$scope.result);
    },function(error){
      console.log("error:"+error);
    });
    //DataService.getContentById(id).then(methodSuccess(response), methodError(response));  //ajax promise
        //success, error
  };

  this.method2 = function(id){
        console.log("method2 called");
        DataService.getContentById(id).then(methodSuccess,methodError);
          //.then(function(response){},function(error){} );
  };

  this.method3 = function(id){
    console.log("method3 called");
    $scope.result = id;
    console.log("scope.result:"+$scope.result);
  };

  //broadcast $watch $on

}



module.exports = MyController;
