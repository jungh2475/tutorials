//npm install -g browserify..........browserify main.js > bundle.js

//.....npm install --save angular angular-route browserify  //npm init, sudo npm install npm -g, npm -v
//node for browserify : browserify --entry ./app.js --outfile ./bundle.js
'use strict';

var angular = require('angular');
var DataService = require('./DataService');
var MyController = require('./MyController');

//var dService = new DataService(angular.$http);
//var mController = new MyController(angular.$scope, dService);

//<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>

var app=angular.module('appName',[]);
//app.config value //sw version, release date, domain url 
app.service('DataService', DataService);
app.controller('MyController', MyController);
//app.service(DataService);
//app.controller(MyController);
