var DataService = require('./DataService');
//var $http=require()
var dService = new DataService($http);
//var dService=new DataService();


function testDataService1(){
    console.log("test1 starting....");

    dService.getContentById(123).then(function(result){
      console.log("success_result:"+result);
    },function(error){
      console.log("error:"+error);
    });
}

function testDataService2(){

    console.log("test2 starting....");

    var success = function(result){
      console.log("success:"+result);
    };
    var errored = function(error){
      console.log("error:"+error);
    };


    dService.getContentById2(3).then(success, errored);
    console.log("test2 completed");


}

function testDataService3(){
    console.log("test3 starting....");

    var result = dService.getContentById3(24);
    console.log("test3 result:"+result);
}

testDataService3();
testDataService2();
testDataService1();
