<?php
require('/Users/hyun/wp2/wp-content/wp_php/wp-data.php');

$wpdata=new WpData();

//1.user
$result = $wpdata->getUserbyId(1);
echo "\n"."test user info"."\n";
print_r($result);

//2.post

//setPost
$post_test1 = new WpPost();
$post_test1->title = 'POSTTEST1';
$post_test1->createdWhen = '';
$post_test1->img_url = 'http://setpost_test.com';
$post_test1->keyword = 'Hello';
$post_test1->num_shares = 3;
$post_test1->category = array(2);
$postId = $wpdata->setPost($post_test1);
echo "\n"."saved id is "."\n".$postId;


//getPostById
$result =$wpdata->getPostById($postId);
echo "\n"."test post info "."\n".$result->title;
print_r($result->category);
print_r($result->keyword);

//removePost
//이미 지워진 포스트일 경우 result_3 :0
//제거 성공시 result_3 : 1

$result = $wpdata->removePost($postId);
echo "\n".$result;


//getPostById
$result =$wpdata->getPostById($postId);
echo "\n"."test post info "."\n".$result->title;


//setCategorySlug
$result = $wpdata->setCategorySlug('laptop');

//getCategoryIdBySlug
$result = $wpdata->getCategoryIdBySlug('laptop');
echo "\n"."test Category id is"."\n".$result;

//updateCategorySlug
$result = $wpdata->updateCategorySlug($result,'tablet');

//getCategoryIdBySlug
$result = $wpdata->getCategoryIdBySlug('tablet');
echo "\n"."test Category id is"."\n".$result;

//3.idea

//setIdea
//post_type : idea

$idea_test = new WpIdea();
$idea_test -> title = 'IDEATEST';
$idea_test->createdWhen = '';
$idea_test->img_url = 'http://abafgasdfc.com';
$idea_test->keyword = 'hello';
$idea_test->num_shares = 3;
$idea_test->post_type ='idea';
$idea_test->category = array(4);
$postId = $wpdata->setIdea($idea_test);
echo "\n"."saved idea ID is "."\n".$postId;


//getIdeaById
$result = $wpdata->getIdeaById($postId);
echo "\n"."result title is "."\n".$result->title;
echo "\n"."result post type is "."\n".$result->post_type;
print_r($result->category);

//setIdeaCategorySlug
$result = $wpdata->setIdeaCategorySlug('gray');

//getIdeaCategoryIdBySlug
$result = $wpdata->getIdeaCategoryIdBySlug('gray');
echo "\n"."test Category id is"."\n".$result;

//updateIdeaCategorySlug
$result = $wpdata->updateIdeaCategorySlug($result,'yolo');
echo "\n"."test updateIdeaCategorySlug is "."\n".print_r($result);

//getIdeaCategoryIdBySlug
$result = $wpdata->getIdeaCategoryIdBySlug('yolo');
echo "\n"."test Category id is"."\n".$result."\n";



//4.comment : 여기는 wp function들을 사용해서 출력하는 예시들을 보여주자 , category,...
