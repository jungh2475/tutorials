<?php

require('/Users/hyun/wp2/wp-load.php');
require('/Users/hyun/wp2/wp-content/wp_php/user.php');
require('/Users/hyun/wp2/wp-content/wp_php/post.php');
require('/Users/hyun/wp2/wp-content/wp_php/idea.php');

class Wpdata {

  function __construct() {

  }
  //1.user
  public function getUserbyId($userId) {
    $result = get_user_by('id',$userId);
    $user = new WpUser();
    $user ->user_id = $result->data->ID;
    $user ->user_login = $result->data->user_login;
    $user ->email = $result->data->user_email;
    $user ->name  = $result->data->user_nicename;

    return $user;
  }


  //2.post
  public function getPostById($postId){

    $result = get_post($postId);
    $terms = wp_get_post_terms($postId,'category');
    $post = new WpPost();
    $post->postId = $result->ID;
    $post->title = $result->post_title;
    $post->createdWhen = $result->post_date;
    $post ->description = $result->post_content;
    $post ->author =  $result ->post_author;
    $post->updatedWhen = $result ->post_modified;
    $post->post_type = $result ->post_type;
    $post->category = $terms;
    $post->img_url = get_post_meta($post->postId,'img_url',false);
    $post->keyword = get_post_meta($post->postId,'keyword',false);
    $post->num_shares = get_post_meta($post->postId,'num_shares',false);
    $post->num_likes = get_post_meta($post->postId,'num_likes',false);
    $post->num_views = get_post_meta($post->postId,'num_views',false);
    $post->url = get_post_meta($post->postId,'url',false);


    return $post;

  }
/*
  public function getPostsByCategoryName($categoryName){
      $= get_cat_ID( $cat_name )
      if($category_id == 0){}
      $args;
      $postIds= get_post($args); //arrayㄹ로 나옴 그래서
      //$post=getPostById($postIds.pop);
      $posts.add($post);
      return $posts;

  }
  //.......
  */
  public function getCategoryIdBySlug($slug){

    $category = get_category_by_slug($slug);
    $category_id = $category->term_id;
    return $category_id;

  }
  public function setCategorySlug($slug){
    wp_insert_term(/*termname*/$slug,'category',array(
      'slug' => $slug
    ));
  }

  public function updateCategorySlug($termId, $slug){
    wp_update_term($termId,'category',array(
      'slug' => $slug
    ));
  }

  public function setPost($post) {

    if (!is_a($post, 'WpPost')) {
        echo "yes, you are not Post object...existing";
        return -1;

    }

    if(!isset($post->postid)){

      $input = array(
        'post_author' =>$post->author,
        'ID' =>$post->postId,
        'post_title' =>$post->title,
        'post_date' =>$post->createdWhen,
        'post_content' =>$post->description,
        'post_modified' =>$post->updatedWhen,
        'post_type' =>$post->post_type

      );

      $postId = wp_insert_post($input,true);

    }else{
      $input = array(
        'post_author' =>$post->author,
        'ID' =>$post->postId,
        'post_title' =>$post->title,
        'post_date' =>$post->createdWhen,
        'post_content' =>$post->description,
        'post_modified' =>$post->updatedWhen,
        'post_type' =>$post->post_type

      );
      $postId = wp_update_post($input);
    }

    if ( ! add_post_meta($postId,"img_url", $post->img_url))
    {
    update_post_meta($postId,"img_url", $post->img_url);
    }
    if ( ! add_post_meta($postId,"keyword", $post->keyword))
    {
    update_post_meta($postId,"keyword", $post->keyword);
    }
    if ( ! add_post_meta($postId,"num_shares", $post->num_shares))
    {
    update_post_meta($postId,"num_shares", $post->num_shares);
    }
    if ( ! add_post_meta($postId,"num_likes", $post->num_likes))
    {
    update_post_meta($postId,"num_likes", $post->num_likes);
    }
    if ( ! add_post_meta($postId,"num_views", $post->num_views))
    {
    update_post_meta($postId,"num_views", $post->num_views);
    }
    if ( ! add_post_meta($postId,"url", $post->url))
    {
    update_post_meta($postId,"url", $post->url);
    }

    $terms=$post->category;
    wp_set_post_terms($postId, $terms, 'category');
    return $postId;
  }

  public function removePost($postId){
     if(get_post($postId)) {
    wp_delete_post($postId);
    return 1;
    }else{return 0;}
  }
  //3.idea
  public function getIdeaById($postId){
    $result = get_post($postId);
    $terms = wp_get_post_terms($postId,'idea_category');
    $post = new WpIdea();
    $post->postId = $result->ID;
    $post->title = $result->post_title;
    $post->createdWhen = $result->post_date;
    $post ->description = $result->post_content;
    $post ->author =  $result ->post_author;
    $post->updatedWhen = $result ->post_modified;
    $post->post_type = $result ->post_type;
    $post->category = $terms;
    $post->img_url = get_post_meta($post->postId,'img_url',false);
    $post->keyword = get_post_meta($post->postId,'keyword',false);
    $post->num_shares = get_post_meta($post->postId,'num_shares',false);
    $post->num_likes = get_post_meta($post->postId,'num_likes',false);
    $post->num_views = get_post_meta($post->postId,'num_views',false);
    $post->url = get_post_meta($post->postId,'url',false);


  return $post;
  }
  //public function getIdeasByCategoryName($categoryName){}
  public function getIdeaCategoryIdBySlug($slug){  //$term_id
    //get_termsBy
    $category = get_term_by('slug',$slug,'idea_category');
    $category_id = $category->term_id;
    return $category_id;
  }


  public function setIdeaCategorySlug($slug){
    wp_insert_term(/*termname*/$slug,'idea_category',array(
      'slug' => $slug
    ));
  }
  public function updateIdeaCategorySlug($termId, $slug){
    wp_update_term($termId,'idea_category',array(
      'slug' => $slug
    ));
  }
  public function setIdea($post) {
    if(!is_a($post, 'WpIdea')) {
      echo "you are not Idea Object.....existing";
      return -1;
    }
    if(!isset($post->postid)){

      $input = array(
        'post_author' =>$post->author,
        'ID' =>$post->postId,
        'post_title' =>$post->title,
        'post_date' =>$post->createdWhen,
        'post_content' =>$post->description,
        'post_modified' =>$post->updatedWhen,
        'post_type' =>$post->post_type

      );

      $postId = wp_insert_post($input,true);

    }else{
      $input = array(
        'post_author' =>$post->author,
        'ID' =>$post->postId,
        'post_title' =>$post->title,
        'post_date' =>$post->createdWhen,
        'post_content' =>$post->description,
        'post_modified' =>$post->updatedWhen,
        'post_type' =>$post->post_type

      );
      $postId = wp_update_post($input);
    }

    if ( ! add_post_meta($postId,"img_url", $post->img_url))
    {
    update_post_meta($postId,"img_url", $post->img_url);
    }
    if ( ! add_post_meta($postId,"keyword", $post->keyword))
    {
    update_post_meta($postId,"keyword", $post->keyword);
    }
    if ( ! add_post_meta($postId,"num_shares", $post->num_shares))
    {
    update_post_meta($postId,"num_shares", $post->num_shares);
    }
    if ( ! add_post_meta($postId,"num_likes", $post->num_likes))
    {
    update_post_meta($postId,"num_likes", $post->num_likes);
    }
    if ( ! add_post_meta($postId,"num_views", $post->num_views))
    {
    update_post_meta($postId,"num_views", $post->num_views);
    }
    if ( ! add_post_meta($postId,"url", $post->url))
    {
    update_post_meta($postId,"url", $post->url);
    }
    $terms = $post->category;
    wp_set_post_terms($postId, $terms, 'idea_category');
    // idea category 아닐경우 null
    return $postId;
  }



}

?>
