<?php

/*

사용법 : curl -X POST http://192.168.219.116/webapi/index2.php -d '{a:b}'

[url mapping in apache2]

1) apache httpd.conf, hosts.conf
    Alias "/webapi" "/var/web"  : from Document Root, mod_alias enabled?
    Alias /webapi /home/ubuntu/wordpress-lamp-1/src/main/functions
    Redirect permanent "/foo/"   "http://www.example.com/bar/"

2).htaccess current folder
    이 폴더로 들어 오는 내용들이 여기로 처리되게 하기 위해서 .htaccess 파일을 현재 디렉토리에 두어서 rest.php로 다 들어 오도록 한다

    .htaccess file
    <IfModule mod_rewrite.c>
        RewriteEngine On
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule ^((?s).*)$ rest.php?_url=/$1 [QSA,L,NC]
    </IfModule>
    NC:not-case-senstitive, L:no further process if matched, QSA:namedCapture will be appended


더 좋은 구성을 위해 다음 framework도 사용해도 된다
//https://docs.phalconphp.com/en/3.0.0/reference/tutorial-rest.html
use Phalcon\Mvc\Micro; $app = new Micro();
$app->get("/api/robots/search/{name}",function ($name) {}); .....$app->put... $app->handle();

*/

//like counter up, share counter up
function upCounter($args){
  //like나, share의 숫자를 하나 더 올린다
  return 1;
}


function updatePost($post){
  //
  return 1;
}


function getPost($post_id){
  //
  return 'hi';
}

/*
  inputs: $_SERVER, $_REQUEST, $_GET, $_POST : test with isset, empty() , var_dump($_POST)
*/

$request=explode('/',trim($_SERVER['PATH_INFO'],'/'));
$method=$_SERVER['REQUEST_METHOD']; //get, post, put, delete
$url='php://input';$contents = file_get_contents($url);$contents = utf8_encode($contents);
$data=json_decode($contents,true);
//$jsonError=json_last_error(), $data['field'];  출처: http://terminaldogma.tistory.com/52

echo "hello world";

if($data->action == 'upCount'){
  $result=upCounter(array());
  if($result==1) { $response='json_response'; }

}

//.......각각의 할 일들을 적어서 동작을 시킨다. 

echo $response;
