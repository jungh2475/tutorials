this is search page



<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Shape
 * @since Shape 1.0
 */

get_header(); ?>



<div id="content" class="site-content" role="main">
  <?php if ( have_posts() ) : ?>

            <?php while ( have_posts() ) : the_post(); ?>

                <?php shape_content_nav( 'nav-above' ); ?>

                <?php get_template_part( 'content', 'single' ); ?>

                <?php shape_content_nav( 'nav-below' ); ?>

                <?php
                    // If comments are open or we have at least one comment, load up the comment template
                    if ( comments_open() || '0' != get_comments_number() )
                        comments_template( '', true );
                ?>

            <?php endwhile; // end of the loop. ?>
  <?php else : ?>

                        <?php get_template_part( 'no-results', 'search' ); ?>

  <?php endif; ?>

</div><!-- #content .site-content -->

<?php get_search_form(); ?>

<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
        <label for="s" class="assistive-text"><?php _e( 'Search', 'shape' ); ?></label>
        <input type="text" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php esc_attr_e( 'Search &hellip;', 'shape' ); ?>" />
        <input type="submit" class="submit" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'shape' ); ?>" />
</form>


<?php
if(isset($_POST['send'])) {  }

  // wp_safe_redirect("url");
  wp_redirect( home_url('/thank-you/') ); exit;
?>


<?php
<?php

global $query_string;

$search_query = wp_parse_str( $query_string );
$search = new WP_Query( $search_query );

global $wp_query;
$total_results = $wp_query->found_posts;


?>


?>
