<?php


/*
  1. load parent style & custom js scripts
  2. register custom post type 'idea' & taxanomy('idea_category')
  3. shortcode들을 등록해 놓는다 (require로 가져옴)

  만약에 child theme이면 필요한 추가 함수가 있을경우 자식테마의 functions.php에만 추가

*/

function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

function enqueue_custom_scripts(){
  //wp_enqueue_script('jquery',get_template_directory_uri().'/scripts/jquey-min.js' );
  wp_enqueue_script('view-click',get_template_directory_uri().'/scripts/view.js' );
}

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
add_action( 'wp_enqueue_scripts', 'enqueue_custom_scripts' );

/*
   init 하여 성공하면 menubar에 idea 메뉴가 보인다
*/

function idea_registration(){

  $labels = array(
  'name'               => _x( 'Idea', 'post type general name' ),
  'singular_name'      => _x( 'Idea', 'post type singular name' ),
  'add_new'            => _x( 'Add New', 'idea' ),
  'add_new_item'       => __( 'Add New Idea' ),
  'edit_item'          => __( 'Edit Idea' ),
  'new_item'           => __( 'New Idea' ),
  'all_items'          => __( 'All Idea' ),
  'view_item'          => __( 'View Idea' ),
  'search_items'       => __( 'Search Idea' ),
  'not_found'          => __( 'No idea found' ),
  'not_found_in_trash' => __( 'No idea found in the Trash' ),
  'parent_item_colon'  => '',
  'menu_name'          => 'Ideas'
  );
  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'menu_position'      => 4,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'supports'           => array('title', 'editor', 'thumbnail', 'excerpt', 'comments','custom-fields'),
    'taxonomies'         => array('idea_category','category'),
    'has_archive'        => true,
    'hierarchical'       => false
  );
  $labels2 = array(
    'name'              => _x( 'Idea Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Idea Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Idea Categories' ),
    'all_items'         => __( 'All Idea Categories' ),
    'parent_item'       => __( 'Parent Idea Category' ),
    'parent_item_colon' => __( 'Parent Idea Category:' ),
    'edit_item'         => __( 'Edit Idea Category' ),
    'update_item'       => __( 'Update Idea Category' ),
    'add_new_item'      => __( 'Add New Idea Category' ),
    'new_item_name'     => __( 'New Idea Category' ),
    'menu_name'         => __( 'Idea Category' ),
  );
  $args2 = array(
    'hierarchical'      => true,
		'labels'            => $labels2,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,

  );
  $result2 = register_taxonomy('idea_category',null,$args2);
  $result = register_post_type('idea',$args);
  register_taxonomy_for_object_type( 'idea_category', 'idea' );
  // register_taxonomy('idea_category','idea');
}


add_action( 'init','idea_registration');


/*
../author/...로 되어있느느 것을 member로 바꾼다..그래도 사용은 author.php이다

*/

function change_author_permalinks()
{
    global $wp_rewrite;
    $wp_rewrite->author_base = 'member'; // Change 'member' to be the base URL you wish to use
    $wp_rewrite->author_structure = '/' . $wp_rewrite->author_base. '/%author%';
}
add_action('init','change_author_permalinks');

//TODO: 화면 출력을 위한  shortcode 들 추가하여 등록하기
//require('./shortcodes.php');



//custom login actions redirect .....https://themetrust.com/build-custom-wordpress-login-page/
/* Main redirection of the default login page */
function redirect_login_page() {
	$login_page  = home_url('/login/');
	$page_viewed = basename($_SERVER['REQUEST_URI']);

	if($page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
		wp_redirect($login_page);
		exit;
	}
}
add_action('init','redirect_login_page');
/*
function redirect_logged_in_user( $redirect_to = null ) {
    $user = wp_get_current_user();
    if ( user_can( $user, 'manage_options' ) ) {
        if ( $redirect_to ) {
            wp_safe_redirect( $redirect_to );
        } else {
            wp_redirect( admin_url() );
        }
    } else {
        wp_redirect( home_url( 'member-account' ) );
}

function redirect_to_custom_login() {
    if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
        $redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : null;

        if ( is_user_logged_in() ) {
            redirect_logged_in_user( $redirect_to );
            exit;
        }

        // The rest are redirected to the login page
        $login_url = home_url( 'member-login' );
        if ( ! empty( $redirect_to ) ) {
            $login_url = add_query_arg( 'redirect_to', $redirect_to, $login_url );
        }

        wp_redirect( $login_url );
        exit;
    }
}

add_action( 'login_form_login', 'redirect_to_custom_login');
*/

/* Where to go if a login failed */
function custom_login_failed() {
	$login_page  = home_url('/login/');
	wp_redirect($login_page . '?login=failed');
	exit;
}
add_action('wp_login_failed', 'custom_login_failed');

/* Where to go if any of the fields were empty */
function verify_user_pass($user, $username, $password) {
	$login_page  = home_url('/login/');
	if($username == "" || $password == "") {
		wp_redirect($login_page . "?login=empty");
		exit;
	}
}
add_filter('authenticate', 'verify_user_pass', 1, 3);

/* What to do on logout */
function logout_redirect() {
	$login_page  = home_url('/login/');
	wp_redirect($login_page . "?login=false");
	exit;
}
add_action('wp_logout','logout_redirect');
//add_filter( 'authenticate', array( $this, 'maybe_redirect_at_authenticate' ), 101, 3 );
