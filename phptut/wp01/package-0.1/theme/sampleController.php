<html>

현재 페이지가 어떤 종류의 페이지 인지 알아 내어서 적합한 화면을 그리거나, 다른 페이지로 redirect 시켜준다

<?php

  //상태를 알아 내는데 쓰이는 함수들 - controller
  is_page();  //is_page('products')
  is_home();
  is_front_page(); //under menu reading
  is_single()
  is_archive()
  is_404()
  is_page_template(); //is_page_template( 'page-templates/front-page.php' )

  //post_id 알아내는 함수
  get_the_ID() //이를 가지고 view 숫자를 올린다 

  //화면에 출력하는 print 함수들
  get_header( 'shop' );

  comments_template( '', true );

?>






</html>
