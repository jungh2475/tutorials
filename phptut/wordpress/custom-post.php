<?php

/*

https://www.smashingmagazine.com/2012/11/complete-guide-custom-post-types/
https://www.smashingmagazine.com/2012/01/create-custom-taxonomies-wordpress/

      wp_insert_post(), register_post_type() post_type = post, page, attachment (defaults)
      see http://mysite.com/customposttype/
      add_action( 'init', -> register_post_type()
      add_action( 'init', 'my_taxonomies_product'->register_taxonomy( 'idea_category', 'product', $args );
      add_filter( 'post_updated_messages....) add_action( 'contextual_help', ...return $contextual_help)
      add_action( 'add_meta_boxes', ...add_meta_box(


custom taxanomy : taxonomy-{taxonomy}-{slug}.php  >  taxonomy-{taxonomy}.php  > taxonomy.php
register_taxonomy( String $taxonomy, $object_type(custom_post_type), $args ); see https://codex.wordpress.org/Function_Reference/register_taxonomy


show contents
archive-[post_type].php
$args = array(
      'post_type' => 'product',
      'tax_query' => array(
        array(
          'taxonomy' => 'product_category',
          'field' => 'slug',
          'terms' => 'boardgames'
        )
      )

$products = new WP_Query( $args );
    if( $products->have_posts() ) {
      while( $products->have_posts() ) {
        $products->the_post();
        ?>
          <h1><?php the_title() ?></h1>
          <div class='content'>
            <?php the_content() ?>
          </div>
        <?php
      }
    }
    else {
      echo 'Oh ohm no products!';
    }


*/

/* functions.php */
function my_custom_post_idea() {
  $args = array();
  register_post_type( 'idea', $args );
}
add_action( 'init', 'my_custom_post_idea' );

function my_custom_post_idea() {
  $labels = array(
    'name'               => _x( 'Ideas', 'post type general name' ),
    'singular_name'      => _x( 'Idea', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'idea' ),
    'add_new_item'       => __( 'Add New Idea' ),
    'edit_item'          => __( 'Edit Idea' ),
    'new_item'           => __( 'New Idea' ),
    'all_items'          => __( 'All Ideas' ),
    'view_item'          => __( 'View Idea' ),
    'search_items'       => __( 'Search Ideas' ),
    'not_found'          => __( 'No Idea found' ),
    'not_found_in_trash' => __( 'No Ideas found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Ideas'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our ideas and description',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'has_archive'   => true,
  );
  register_post_type( 'idea', $args );
}
add_action( 'init', 'my_custom_post_idea' );

function my_updated_messages( $messages ) {
  global $post, $post_ID;
  $messages['idea'] = array(
    0 => '',
    1 => sprintf( __('Idea updated. <a href="%s">View idea</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Idea updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Idea restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Idea published. <a href="%s">Idea idea</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Idea saved.'),
    8 => sprintf( __('Idea submitted. <a target="_blank" href="%s">Preview idea</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Idea scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview idea</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Idea draft updated. <a target="_blank" href="%s">Preview idea</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'my_updated_messages' );

function my_contextual_help( $contextual_help, $screen_id, $screen ) {
  if ( 'product' == $screen->id ) {

    $contextual_help = '<h2>Ideas</h2>
    <p>Ideas show the details of the items that we sell on the website. You can see a list of them on this page in reverse chronological order - the latest one we added is first.</p>
    <p>You can view/edit the details of each product by clicking on its name, or you can perform bulk actions using the dropdown menu and selecting multiple items.</p>';

  } elseif ( 'edit-product' == $screen->id ) {

    $contextual_help = '<h2>Editing ideas</h2>
    <p>This page allows you to view/modify idea details. Please make sure to fill out the available boxes with the appropriate details (image, ) and <strong>not</strong> add these details to the  description.</p>';

  }
  return $contextual_help;
}
add_action( 'contextual_help', 'my_contextual_help', 10, 3 );

function my_taxonomies_product() {
  $labels = array(
    'name'              => _x( 'Product Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Product Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Product Categories' ),
    'all_items'         => __( 'All Product Categories' ),
    'parent_item'       => __( 'Parent Product Category' ),
    'parent_item_colon' => __( 'Parent Product Category:' ),
    'edit_item'         => __( 'Edit Product Category' ),
    'update_item'       => __( 'Update Product Category' ),
    'add_new_item'      => __( 'Add New Product Category' ),
    'new_item_name'     => __( 'New Product Category' ),
    'menu_name'         => __( 'Product Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'product_category', 'product', $args );
}
add_action( 'init', 'my_taxonomies_product', 0 );


add_action( 'add_meta_boxes', 'product_price_box' );
function product_price_box() {
    add_meta_box(
        'product_price_box',
        __( 'Product Price', 'myplugin_textdomain' ),
        'product_price_box_content',
        'product',
        'side',
        'high'
    );
}

//DEFINING THE CONTENT OF THE META BOX
function product_price_box_content( $post ) {
  wp_nonce_field( plugin_basename( __FILE__ ), 'product_price_box_content_nonce' );
  echo '<label for="product_price"></label>';
  echo '<input type="text" id="product_price" name="product_price" placeholder="enter a price" />';
}
