<? php
/* https://www.smashingmagazine.com/2016/01/create-customize-wordpress-child-theme/
   https://code.tutsplus.com/tutorials/developing-your-first-wordpress-theme-day-1-of-3--wp-19
   https://code.tutsplus.com/tutorials/developing-your-first-wordpress-theme-day-2-of-3--wp-31
   https://code.tutsplus.com/tutorials/developing-your-first-wordpress-theme-day-3-of-3--wp-135
   https://www.taniarascia.com/developing-a-wordpress-theme-from-scratch/

*/
//instead of style.css : @import url("../twentyfifteen/style.css");
//do not copy the full contents of your parent theme’s functions.php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}
