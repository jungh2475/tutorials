<?php

/*

  아주간단한 방법은 다음과 같이
  http://www.hongkiat.com/blog/wordpress-custom-loginpage/amp/
  page를 만들고 wp_login_form( $args );, 자동 redirect설정 add_action('init',if( $page_viewed == "wp-login.php"wp_redirect($login_page);
  add_action('wp_logout','wp_redirect( $login_page . "?login=false" );


  1. replace the existing wp-login.php
  https://code.tutsplus.com/tutorials/build-a-custom-wordpress-user-flow-part-1-replace-the-login-page--cms-23627
  2. 신규 사용자 등록 (google bot 사용)
https://code.tutsplus.com/tutorials/build-a-custom-wordpress-user-flow-part-2-new-user-registration--cms-23810
  3. 비빌번호 reset
https://code.tutsplus.com/tutorials/build-a-custom-wordpress-user-flow-part-3-password-reset--cms-23811
  4.social login sso
https://www.cloudways.com/blog/how-to-add-social-login-plugin-wordpress/
https://hybridauth.github.io/developer-ref-user-authentication.html


*/



//plugin activation(활성화 될떄) 호출이 된다
register_activation_hook( __FILE__, array( 'Personalize_Login_Plugin', 'plugin_activated' ) );
//shortcode로 form 내용 준비
add_shortcode( 'custom-login-form', array( $this, 'render_login_form' ) );
//준비화면 template와 변수 mapping 도 준비
private function get_template_html( $template_name, $attributes = null ) {}
//로그인 login action이 생기면 redirect
add_action( 'login_form_login', wp_get_current_user(); redirect_logged_in_user or wp_redirect( $login_url ); );
//로그인 체크시에 추가 변형
add_filter( 'authenticate', wp_redirect_if_error, 101, 3)
//logout때
add_action( 'wp_logout', wp_safe_redirect( $redirect_url );exit; );

add_filter( 'login_redirect', wp_validate_redirect( $redirect_url, home_url() ););

add_shortcode for : [custom-register-form]
//Redirect the User to Our New Registration Page
add_action( 'login_form_register','GET' == $_SERVER['REQUEST_METHOD'] ifnot ( is_user_logged_in() wp_redirect( home_url( 'member-register' ) );
//Call the Registration Code When a User Submits the Form
add_action( 'login_form_register',$first_name = sanitize_text_field( $_POST['first_name'] );$result = $this->register_user(wp_redirect( $redirect_url );exit;
//
if ( isset( $_REQUEST['register-errors'] ) ) {
//adding chapcha
add_filter( 'admin_init' register_setting( 'general', 'personalize-login-recaptcha-site-key' );
// Retrieve recaptcha key
$attributes['recaptcha_site_key'] = get_option( 'personalize-login-recaptcha-site-key', null );
add_action( 'wp_print_footer_scripts', array( $this, 'add_captcha_js_to_footer' ) );
private function verify_recaptcha() {
add_action( 'login_form_lostpassword',

/*
중요 체크할 변수들

isset( $_REQUEST['login'], $_SERVER['REQUEST_METHOD'] === 'POST' or 'GET'
 $redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : null;
$user = wp_get_current_user();, is_wp_error( $user )
$login_url = home_url( 'member-login' );
// Check if user just logged out
$attributes['logged_out'] = isset( $_REQUEST['logged_out'] ) && $_REQUEST['logged_out'] == true;

*/


/*
    short_code output에서 유용한 함수
    ob_start();
    //do_action( 'personalize_login_before_' . $template_name );
    require( 'templates/' . $template_name . '.php');
    //do_action( 'personalize_login_after_' . $template_name );
    $html = ob_get_contents();
    ob_end_clean();
    return $html;

*/

/*
  user 변수들 (등록시 필요한것들)
  $user_data = array(
        'user_login'    => $email,
        'user_email'    => $email,
        'user_pass'     => $password,
        'first_name'    => $first_name,
        'last_name'     => $last_name,
        'nickname'      => $first_name,
    );

    $user_id = wp_insert_user( $user_data );
    wp_new_user_notification( $user_id, $password );
*/

/*  php redirect post

form submit을 한 url의 php에서 header에 redirect_url을 넣어주면 결제/로그인후에 다음 페이지로 넘어 갈수가 있다
<form name='fr' action='redirect.php' method='POST'>
header('Location: https://www.xyz.com/page1.php?check=10);
아니면 admin-post.php로 데이터를 ajax로 보내거나 wp_redirect를 써어 다음페이지로 보내준다
*/
