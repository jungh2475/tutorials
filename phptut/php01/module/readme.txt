constrcutor overloading

public function __construct($driver) {
        if (is_string($driver)) {
            $driver = $this->createDriverFromString($driver);
        } elseif (is_array($driver)) {
            $driver = $this->createDriverFromArray($driver);
        }

        if (!$driver instanceof DriverInterface) {
            throw new Exception();
        }
    }


    public function __construct(DbAdapter $dbAdapter, $tableName, $data) {
            $this->dbAdapter = $dbAdapter;
            $this->tableName = $tableName;
            $this->data = $data;
        }



/*  php redirect post

form submit을 한 url의 php에서 header에 redirect_url을 넣어주면 결제/로그인후에 다음 페이지로 넘어 갈수가 있다
<form name='fr' action='redirect.php' method='POST'>
header('Location: https://www.xyz.com/page1.php?check=10);
아니면 admin-post.php로 데이터를 ajax로 보내거나 wp_redirect를 써어 다음페이지로 보내준다 
