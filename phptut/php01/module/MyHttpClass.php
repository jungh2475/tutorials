<?php

//echo "show Global Variables : Request, Server,Get,Post..."."\r\n";


/* php5 global variables:
*   $GLOBALS,$_SERVER,$_POST
*   https://www.w3schools.com/php/php_superglobals.asp:
*   apache에서 php연동이 되면 자동으로 값들이 매핑이 된다.
*/
//var_dump($_POST)."\r\n";  //$_GET["name"]
//echo $_REQUEST;
//$data_raw=file_get_contents('php://input'); //or $url='http://.../.../yoururl/...'; = $_POST or $_GET, # Get JSON as a string
//$obj = json_decode($rawData); //# Get as an object
//echo $obj->access_token;

/*
*   global variables wrapper class :
*   $_SERVER, $_POST, $_GET을 평상시에는 그냥 호출하고, 테스트일때는 Mock을 호출하도록 하는 클래스임.
*
*
*/
class MyHttpClass {

    public $mode;

    //function __construct(){}

    function __construct($m){
      $this->mode = $m;
      //echo "constructed! $mode";
    }

    //$_GET
    public function get($url = '50'){
      if($this->mode == 1){
        //return $_;
      }
      else{

      }
      return "hihi $url you are in mode, ".$this->mode;  // = echo "hihi $url";
      //echo $this->mode;
    }

    //$_POST
        
    //$_SERVER
}
