<?php

interface IWordpress {

    public function getUser($id);
    public function setUser(array $user);
    //$type = post, page, product, idea
    public function getContent($type, $id);
    public function setContent($type, array $content);  //create, update
    public function removeContent($type, $id);   //delete

    public function getMeta($type, $content_id, array $keys);
    public function getMetaKeys($type, $content_id);  // all keys
    public function setMeta($type, $content_id, array $key_values);

}


//확인 방법 , var_dump($a instanceof MyClass); var_dump($a instanceof MyInterface); 이전에는 is_a()가 사용
