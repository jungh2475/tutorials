
import requests
import json

post_url = 'https://www.googleapis.com/urlshortener/v1/url'
key = {"key": "AIzaSyC4d5b2uBM4AI5yQ2mLmO8SlRFXDf_7_JY" }
metadata = {"longUrl": "https://drive.google.com/file/d/0B4ygv8SL7rFiVDVSSms4Q3JfcFk/view?usp=drivesdk"}

response = requests.post(post_url, params=key, data=json.dumps(metadata), headers={'Content-Type': 'application/json'})
print(response.url)
print(response.json())
