'''
    pyDrive 사용? pip install PyDrive with file client_secrets.json
    https://pythonhosted.org/PyDrive/quickstart.html
    from pydrive.auth import GoogleAuth
    from pydrive.drive import GoogleDrive

    gauth = GoogleAuth()
    gauth.LocalWebserverAuth()

    drive = GoogleDrive(gauth)
    file_list = drive.ListFile({'q': "'root' in parents"}).GetList()

    googleSheet  는  gSpread module 활용

    https://developers.google.com/drive/v3/web/quickstart/python
    pip install --upgrade google-api-python-client
    python3 test1.py
    아래 방식은 webBrowser가 있어야함 auth...이것 없이 해야함.

    https://developers.google.com/api-client-library/python/
    1. api key
        - not access any private user data, API key from console, https://console.cloud.google.com/
    2. oauth2
        - Scope(read-write), Refresh and access tokens, Client ID(web, service account) and client secret
        - server-2-server service account: https://console.developers.google.com/permissions/serviceaccounts
        1)
        from oauth2client.contrib.gce import AppAssertionCredentials

        credentials = AppAssertionCredentials(
            'https://www.googleapis.com/auth/sqlservice.admin')
        2)
        from oauth2client.service_account import ServiceAccountCredentials

        scopes = ['https://www.googleapis.com/auth/sqlservice.admin']

        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            '/path/to/keyfile.json', scopes=scopes)
        3)# Authorize server-to-server interactions from Google Compute Engine.
        from oauth2client.contrib import gce

        credentials = gce.AppAssertionCredentials(
            scope='https://www.googleapis.com/auth/devstorage.read_write')
        http = credentials.authorize(httplib2.Http())

        from apiclient.discovery import build

        sqladmin = build('sqladmin', 'v1beta3', http=http_auth)
        response = sqladmin.instances().list(project='exciting-example-123').execute()

    # List my public Google+ activities.
    result = service.activities().list(userId='me', collection='public').execute()
    tasks = result.get('items', [])
    for task in tasks:
        print task['title']
'''



import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/drive-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/drive.metadata.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Drive API Python Quickstart'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'drive-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def main():
    """Shows basic usage of the Google Drive API.

    Creates a Google Drive API service object and outputs the names and IDs
    for up to 10 files.
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)

    results = service.files().list(
        pageSize=10,fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])
    if not items:
        print('No files found.')
    else:
        print('Files:')
        for item in items:
            print('{0} ({1})'.format(item['name'], item['id']))

if __name__ == '__main__':
    main()
