'''

https://www.compose.com/articles/loading-google-analytics-data-to-postgresql-using-python/

'''




#python3 -m pip install google-api-python-client
#python googledata_test.py


'''
from apiclient.discovery import build
service = build('analytics', 'v3', http=http_auth)
service.data().ga().get(....)

'''

import sys
from googleapiclient.errors import HttpError
from googleapiclient import sample_tools
from oauth2client.service_account import ServiceAccountCredentials
from httplib2 import Http
from apiclient.discovery import build

#


def get_api_traffic_query(service):
  return service.data().ga().get(
    ids='ga:xxxxxxx',
    start_date='2014-01-01',
    end_date='2014-01-31',
    metrics='ga:users,ga:sessions',
    dimensions='ga:yearMonth',
#    sort='-ga:yearMonth',
#    filters='ga:pagePath=~signup',
    segment='sessions::condition::ga:hostname!~mongo|app|help|docs|staging|googleweblight',
    start_index='1',
    max_results='25')

#Main
def main():
    #https://developers.google.com/analytics/devguides/reporting/core/v3/quickstart/service-py
    Authenticate and create the service for the Core Reporting API
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
      'Compose-GA-xxxxxxx.json', ['https://www.googleapis.com/auth/analytics.readonly'])
    http_auth = credentials.authorize(Http())
    service = build('analytics', 'v3', http=http_auth)


    # Run the query function using the API service
    traffic_results = get_api_traffic_query(service).execute()

    # Insert each row of the result set
    if traffic_results.get('rows', []):
      for row in traffic_results.get('rows'):
          #pass 여기서 gSheet에 데이터를 저장한다
    else:
      print('No Rows Found')

    print("all done!")


if __name__ == '__main__':
  main()
