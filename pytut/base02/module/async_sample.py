
#synchronous $http
print("first is synchronous")
import requests

def hello():
    return requests.get("http://httpbin.org/get")

print(hello().text)  #response.content, .json(), .encoding, .raw, requests.get(url, headers=headers) post -data


#asyncs(1)
print("async 1 - basic 1 ")

import asyncio
from aiohttp import ClientSession

# two asynchronous operations t
async def hello(url):
    async with ClientSession() as session:
        async with session.get(url) as response:
            response = await response.read()
            print(response)

loop = asyncio.get_event_loop()

loop.run_until_complete(hello("http://httpbin.org/headers"))

#asyncs(2)
print("async 2 - multiple urls")
loop = asyncio.get_event_loop()

tasks = []
url = "http://httpbin.org/headers"
for i in range(5):
    task = asyncio.ensure_future(hello(url.format(i)))
    tasks.append(task)
loop.run_until_complete(asyncio.wait(tasks))

#asyncs(3)
###############################################
print("async 2 - multiple urls and multiple answers important !!!!!")
print("############################################")

import asyncio
from aiohttp import ClientSession

async def fetch(url, session):
    async with session.get(url) as response:
        print("hello")
        return await response.read()

async def run(r):
    url = "http://httpbin.org/headers"
    tasks = []

    # Fetch all responses within one Client session,
    # keep connection alive for all requests.
    async with ClientSession() as session:
        for i in range(r):
            task = asyncio.ensure_future(fetch(url.format(i), session))
            tasks.append(task)

        responses = await asyncio.gather(*tasks)
        # you now have all response bodies in this variable
        print(responses)

def print_responses(result):
    print(result)

loop = asyncio.get_event_loop()
future = asyncio.ensure_future(run(4))
loop.run_until_complete(future)


print("async 2.x - jungh way")
print("#####################################")
#http://stackabuse.com/python-async-await-tutorial/
import aiohttp
import json

loop = asyncio.get_event_loop()
client = aiohttp.ClientSession(loop=loop)

async def get_json(client, url):
    async with client.get(url) as response:
        assert response.status == 200
        return await response.read()

async def main_parse(url, client):
    data1 = await get_json(client, url)
    j= json.loads(data1.decode('utf-8'))
    #for i in j['data']['children']:
    print('DONE:');

tasks = []
task = asyncio.ensure_future(main_parse('http://httpbin.org/headers', client))
tasks.append(task)
loop.run_until_complete(asyncio.wait(tasks))

print("async 3 - with async_timeout")
print("#####################################")
import aiohttp
import asyncio
import async_timeout

async def fetch2(session, url):
    with async_timeout.timeout(10):
        async with session.get(url) as response:

            await response.read() #print(response.read()) # or response.text()
            return await response.release()  #response.read()

async def main2(loop):
    urls =["http://httpbin.org/headers","http://httpbin.org/headers","http://httpbin.org/headers","http://httpbin.org/headers"]
    async with aiohttp.ClientSession(loop=loop) as session:
        tasks = [fetch2(session, url) for url in urls]
        await asyncio.gather(*tasks)

loop= asyncio.get_event_loop()
loop.run_until_complete(main2(loop))

print("########## all completed ###########")  #https://www.blog.pythonlibrary.org/2016/07/26/python-3-an-intro-to-asyncio/
