#logging -console,file, url
#http://ourcstory.tistory.com/97 http://gyus.me/?p=418
import logging
import logging.handlers

logging.basicConfig(filename='./test.log',level=logging.DEBUG)
 #logging.basicConfig(level=logging.DEBUG)
logging.info("I told you so") #debug, info, warning, error, critical
logging.warning("Watch out!")

# 1. 로거 인스턴스를 만든다
logger = logging.getLogger('mylogger')
# 포매터를 만든다
fomatter = logging.Formatter('[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s > %(message)s')

# 2. 스트림과 파일로 로그를 출력하는 핸들러를 각각 만든다.
#fileHandler = logging.FileHandler('./myLoggerTest.log')
fileMaxByte = 1024 * 1024 * 10 #10MB
fileHandler = logging.handlers.RotatingFileHandler(filename, maxBytes=fileMaxByte, backupCount=10)
streamHandler = logging.StreamHandler()
# 각 핸들러에 포매터를 지정한다.
fileHandler.setFormatter(fomatter)
streamHandler.setFormatter(fomatter)

# 3. 1번에서 만든 로거 인스턴스에 스트림 핸들러와 파일핸들러를 붙인다.
logger.addHandler(fileHandler)
logger.addHandler(streamHandler)

# 4. 로거 인스턴스로 로그를 찍는다.
logger.setLevel(logging.DEBUG)

#LogRecord(name, level, pathname, lineno, msg, args, exc_info, func=None, sinfo=None)
