#pytest -q test_sample.py or pytest -q --cmdopt=type2, https://docs.pytest.org/en/latest/example/simple.html
import pytest

'''
    testreport
    1. testpackage를 등록한다
    2. testcase를 실행 한다 : 각 method안에서 test.report(testcase_id, pass_fail, (time,who)), testreport.commit();
    testcase sw ver, package-language(python or else, target & testcase sw name & repository name)
    testcase id(hash), title : git testcase commit it


    ------------------------------
    testname(title) 읽는 방법 ????????
        - describe("tc_title", function(....this.)), before/each:
    assert 결과를 읽는 방법

    testcase_titles[] : 반드시 변수로 적어서 할당 되게 하자
    assert.equal을 wrapper로 만들어서 사용해야 한다.
    이렇게 두개를 가지고 testReport를 만든다. .......

    (포기) listener, spy 장착 : testcase name, test result(pass?fail) : pytest, jasmine
        - pytest -> this

    (실제 사용례)
        import kew_reporter
        packages = {}
        kReporter=kew_reporter("init_url",packages)
        tc_titles = ["abc", "ddd"]
        def test_xxx():
            passfail = assert(expected, result)
            kReporter.add(tc_titles[3],passfail)
        -----
        kReporter.commit()

    ------------------------------
    testreport를 cli로 실행하는 법

    (javascript) jasmine
    (python) pytest -q? test.py -testrunner=record?
    (java)
    (php) phputil
    (bash_script)

    아니면 test console 출력을 넣어서 보고서로 만들게 하던가 .....

    -------------------------------
    git id를 읽는법 :testcase:package 번호 추적
    git rev-parse --short HEAD = git show --oneline -s ->commit_id, message, git status
    bash_script get the first column : | awk '{print $1}' or cut -d' ' -f1 //$2하니깐 문자열 첫단락만 가져오네



'''


import pytest

def test_zero_division():
    with pytest.raises(ZeroDivisionError):
        1 / 0

def test_2():
    assert a % 2 == 0, "value was odd, should be even"
