

#import mysql
import json

class Reporter:

    def __init__(self):
        self.mode = "test"
        #self.mysql = mysql
        self.reportlist= []  # arraylist [], dictionary {}

    def add(self, id, result):
        temp = {"id":id, "timestamp": 21257361, "result":result}
        self.reportlist.append(temp)

    def commit_report(self):
        #mysql connection syn then try ... async imlementation
        result = {"timestamp":364767, "testreport":self.reportlist}
        print(result)
        #pass

if __name__ == "__main__":
    print("hello")
    reporter = Reporter()
    reporter.add("23879","pass")  #시간 timestamp,
    reporter.add("23880","pass")
    reporter.add("23881","fail")
    #reporter.report()
    #print("reportlist:",reporter.reportlist)
    print("final_report:", reporter.commit_report())
    #jsonStr = json.dumps(reporter.reportlist)
    #print("jsonResult:", jsonStr)
