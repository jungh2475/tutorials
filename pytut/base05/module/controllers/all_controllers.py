from flask import Flask
from flask_celery import make_celery

app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'amqp://localhost//'
#app.config['CELERY_RESULT_BACKEND'] = 'db+mysql://kewtea:1234@localhost/foo'

print(app.import_name)

celery = make_celery(app)

#register subControllers
from esheets.routes import mod

@app.route('/')
def home():
    return 'you are at home'

#monitoring celery -A controller2 worker --loglevel=info
#celery -A application.controllers.routes:celery worker --loglevel=info
@app.route('/process/<name>')
def home2(name):
    reverse2.delay(name)
    return 'you are at home2'

#@celery.task(bind=True) = __main__.reverse2
@celery.task(name='all_controllers.reverse2')
def reverse2(string):
    return string[::-1]
    #return 'hi i am tasked222'

app.register_blueprint(mod, url_prefix='/esheets')
#app.register_blueprint(esheets.routes.mod, url_prefix='/esheets')


if __name__ == '__main__':

    app.run(debug=True)
