from flask import Flask, render_template, request
from werkzeug import secure_filename
app = Flask(__name__)


app.config[‘UPLOAD_FOLDER’]="/path"
app.config[‘MAX_CONTENT_PATH’] = '300' #MB

#webPages
@app.route('/web/<name>, methods = ['GET'])
def landing_page(name):
    #content
    return render_template('index.html', name=name)

#rest-apis
@app.route('/context/<domain>,methods = ['POST'])
def user_context_report():
    //api.save()
    return jsonfy("hi")

class Controller(object)
    def __init__(self, configs):
        self.app=Flask(__name__)
        pass

    def run(self):
        self.app.run(debug = True)
        pass



if __name__ == '__main__':
    app.run(debug = True)

    controller = Controller('')
    controller.run()
