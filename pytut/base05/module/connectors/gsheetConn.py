#https://bitbucket.org/jungh2475/dmalt-elastic-ss/src/35c778cad86bab07fe4efea377fcfddc7a785e21/pysrc/connectors/gsheet_erp.py?at=master&fileviewer=file-view-default
#pandas.csv로 파일 read/write, my_sheet=pd.read_csv(sheet_url, header=0, index_col=0, skiprows=[2])

import gspread
from oauth2client.service_account import ServiceAccountCredentials


class Gsheet(object):

    def __init__(self, sheet_id='1EogyCrngPhZAcxTCA7WCD6c94GLfdAXHlgNrxtCrWVY'):
        self.scope=['https://spreadsheets.google.com/feeds']
        self.credentials = ServiceAccountCredentials.from_json_keyfile_name('My Project 1131-92e23d331383.json', self.scope)
        self.sheet_id=sheet_id
        self.subsheet_name=""
        gc = gspread.authorize(self.credentials)
        self.sheets=gc.open_by_key(sheet_id)
        pass
        #serviceAccount : 867067161628-compute@developer.gserviceaccount.com -> 반드시 문서에서 접속이 되도록 해놓는다. api도 접속 가능하게 ....
        #credentials = ServiceAccountCredentials.from_json_keyfile_name(gsheet_credentialjsonfile, gsheet_scope)
        #wks=gspread.authorize(credentials).open_by_key(gsheet_id).worksheet(gsheet_subsheetName)
        #df=pd.DataFrame(wks.get_all_records(), columns=wks.get_all_values()[0])

    def load_gsheets(self):
        return self.sheets

    def load_subsheet(self, subsheet_name):
        self.subsheet_name = subsheet_name
        return self.sheets.worksheet(subsheet_name)


if __name__ == '__main__':
    sheet_id=''
    subsheet_name=''

    my_sheetsObj=Gsheet(sheet_id)
    my_sub_sheet=my_sheetsObj.load_subsheet(subsheet_name)
    wks=my_sheetsObj.load_subsheet(subsheet_name)
    data = wks.get_all_records()
    cols = wks.get_all_values()[0] #keeps cols in right order
