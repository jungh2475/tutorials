#

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='mysql://root:1234@localhost/kew3db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
db=SQLAlchemy(app)

class userRepo(object):
    pass


class Product(db.Model):
        __tablename__ ='products'
        __table_args__={'mysql_collate':'utf8_general_ci'}
        id=db.Column('id', db.Integer, primary_key=True, unique=True)
        name=db.Column('name',db.String(25)) #db.Unicode
        supplier=db.Column('supplier',db.Unicode)
        tag=db.Column('tag',db.Unicode)

        def __init__(self,id_,name_,supplier_,tag_):
            self.id=id_
            self.name=name_
            self.supplier=supplier_
            self.tag=tag_

if __name__ == "__main__":

    #Product.query.count(), len(products)
    #examples=Example.query.all()... or one=Example.query.filter_by(id=1).first()...db.session.add(Example(x,x))
    #임의의 table에서 필요한 기능들은...insert/update/delete, read.db.Model을 상속해서 필요없음

    products=Product.query.all()
    for product in products:
        print(product.name)

    pass
