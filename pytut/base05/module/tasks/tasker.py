#rabbit-mq
#sudo apt-get install rabbitmq-server -> sudo service rabbitmq-server restart
#sudo rabbitmqctl status
#brew update -> brew install rabbitmq -> (PATH=$PATH:/usr/local/sbin) rabbitmq-server

#pip3 install celery
#celery -A tasker worker --loglevel=info #모니터링 명령어

from celery import Celery
import time

app = Celery('tasker', broker='amqp://localhost//')
#mysql backend : CELERY_RESULT_BACKEND = 'db+mysql://kewtea:1234@localhost/foo'
##app.config['db_url'] =''
#app = Celery('tasker', broker='amqp://localhost//', backend=app.config['db_url'])

@app.task
def reverse(string):
    #time.sleep(10)
    return string[::-1]


if __name__ == '__main__':
    reverse('john')
    result=reverse.delay('james')
    print(result.status) #pending, success or result.ready() ture or false, result.get()
