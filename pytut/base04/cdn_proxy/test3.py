'''

winesoft stone edge server
apache config change api ?  ansible or pxssh, https://httpd.apache.org/docs/2.4/vhosts/mass.html


flask + NGINX and Gunicorn
//nginx.conf

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;
 .......
    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    include /etc/nginx/conf.d/*.conf;
}

///etc/nginx/conf.g/default.conf
server {
   # Define the directory where the contents being requested are stored
   # root /usr/src/app/project/;

   listen 80;
   # server_name xxx.yyy.zzz.aaa
 
   # Configure NGINX to deliver static content from the specified folder
   location /static {
       alias /usr/src/app/project/static;
   }

   # Configure NGINX to reverse proxy HTTP requests to the upstream server (Gunicorn (WSGI server))
   location / {
       # Define the location of the proxy server to send the request to
       proxy_pass http://web:8000;

       # Redefine the header fields that NGINX sends to the upstream server
       proxy_set_header Host $host;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

       # Define the maximum file size on file uploads
       client_max_body_size 5M;
   }
}


'''
