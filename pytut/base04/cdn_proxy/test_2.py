import socket

addr1 = socket.gethostbyname('google.com')
addr2 = socket.gethostbyname('kewtea.com')
addr3 = socket.gethostbyname('www.kewtea.com')
addr4 = socket.gethostbyname('journal.kewtea.com')
addr5 = socket.gethostbyname('naim.kewtea.com')
addr6 = socket.gethostbyname('sphouse.co')
print(addr1, addr2, addr3, addr4, addr5, addr6)

#host 66.249.66.1
reversed_dns1 = socket.gethostbyaddr('172.217.24.46')
reversed_dns2 = socket.gethostbyaddr('35.184.80.132')
reversed_dns3 = socket.gethostbyaddr('130.211.150.104')
# ('crawl-203-208-60-1.googlebot.com', ['1.60.208.203.in-addr.arpa'], ['203.208.60.1'])
reversed_dns1[0]
reversed_dns2[0]
reversed_dns3[0]


from dns import reversename, resolver

rev_name = reversename.from_address('203.208.60.1')
reversed_dns = str(resolver.query(rev_name,"PTR")[0])
# 'crawl-203-208-60-1.googlebot.com.'
