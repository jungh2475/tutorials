

#import gspread, pydrive, swagger-gmail-client
#https://github.com/CanopyIQ/gmail_client  or
# https://developers.google.com/gmail/api/quickstart/python https://developers.google.com/gmail/api/guides/ pip install --upgrade google-api-python-client
#import flask

'''
    gSheet에서 users/contexts(=orders)를 읽어서, 때에 맞는 사람에게 맞는 이메일을 보낸다
       - 구매 후기 작성 요청 혹은 promotion coupon, survy, Q&A 요청
    form_processor -> gSheet에 저장

references: yotpo, Kudobuzz,  crema.me(kr): sdk site popup

'''

from flask import Flask
from flask_mail import Mail, Message

def main():
    app =Flask(__name__)
    app.config['MAIL_SERVER']='smtp.gmail.com'
    app.config['MAIL_PORT'] = 465
    app.config['MAIL_USERNAME'] = 'yourId@gmail.com'
    app.config['MAIL_PASSWORD'] = '*****'
    app.config['MAIL_USE_TLS'] = False
    app.config['MAIL_USE_SSL'] = True
    app.run(debug = True)

#sendMail  pip install Flask-Mail
def sendMail(content):
    mail = Mail(app)
    msg = Message('Hello', sender = 'yourId@gmail.com', recipients = ['id1@gmail.com'])
    #flask-mail.Message(subject, recipients, body, html, sender, cc, bcc,reply-to, date, charset, extra_headers, mail_options, rcpt_options)
    msg.body = "This is the email body"
    mail.send(msg)

def getgmail():  #차라이 밖에 파일로 두는 것이 나을것 같음. 
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)

    results = service.users().labels().list(userId='me').execute()
    labels = results.get('labels', [])

    if not labels:
        print('No labels found.')
    else:
      print('Labels:')
      for label in labels:
        print(label['name'])

if __name__ == '__main__':
   main()
