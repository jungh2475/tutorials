from flask import Flask, request, send_from_directory
import time;
'''
TODO:   1) 여기에 mysql에 연결해서 들어온 내용에 대한 보고를 하는 기능(filelog-"mail_open_log.txt", optional: mysql or gsheet?),
        2) 이메일을 발송할떄 tracking_id를 발급해서 단체 발송 하는 기능...gSheet 명단, id를 보고서 발송...send_mail_gsheet.py
           ..,
        3)
'''

# set the project root directory as the static folder, you can set others.
app = Flask(__name__, static_url_path='')

print('app serving...')

@app.route('/', methods=['GET'])
def hello_world():
    print('hello called\n')
    return 'hello'

@app.route('/js/<path:path>')
def send_js(path):

    return send_from_directory('js', path)

@app.route('/img/<path:path>')   #<int:post_id>'
def send_img(path):
    track = request.args.get('track', default = 1, type = int) #request.form['username']
    print('track_id:',track,'\n')
    text='track_id:'+track
    log_to_file(text)
    return send_from_directory('img', path)
    #url_for('static', filename='foo.bar')
    #render_template('hello.html', name=name), 200 /templates/....html
    #redirect(url_for('login')) ..'login'은 method이름임
    #@app.errorhandler(404), abort(401)

def log_to_file(text):
    f= open("mail_open_log.txt","a+") #"w+")
    localtime = time.asctime( time.localtime(time.time()) )
    #f.write("%s:",text,"\n" % localtime)
    print("",localtime,":",text)
    f.write("",localtime,":",text)
    f.close()


if __name__ == "__main__":
    #app.run()
    app.run(host="192.168.0.2",port=5050, debug=True)
    #app.run(host="192.168.0.2")  or app.run(host='0.0.0.0', port=5050, debug=False)


'''
http://127.0.0.1:5000/
export FLASK_APP=mail_tracking.py
or python3 -m flask run
flask run

or python3 mail_tracking.py

testing with http://127.0.0.1:5000/img/1200px_tvr_s2.jpg?track=36
http://192.168.0.2:5050/img/1px_nff4d00_0.png?track=66

'''
