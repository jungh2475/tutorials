

install python3
  - (mac) brew install python3
  - must check which python3, install of pip3, venv, echo $PYTHONPATH

venv commands
  - python3 -m venv /path/to/new/virtual/environment
  - source v1/bin/activate
  - pip3 install xyz....(python3 -m pip install pytz)
  - pip3 freeze > requirements.txt
  - pip3 install -r requirements.txt
  - deactivate


httpServer
  - python3 -m http.server 8000 --bind 127.0.0.1
