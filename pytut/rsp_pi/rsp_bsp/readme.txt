

installations :

  1)설치 환경을 만든다 :  sdCard에 이미지 만들기 : 그냥 복사해서 만드면 됨
     - wifi 연결, hdmi 연결
  2)계정(token)을 설치해 준다 : 1)copy token, 2)ssh해서 curl -> 토큰을 발행
  3)crontab -e 로 run.py 실행

run.py안의 프로그램들

  0) sysAgent :
      getProcessJobs, SystemServiceReport, auto turn on/off
      configChecker....(wifi, hdmi, audio....)
  1) wifi_sniffer
  2) naimPlayer
  3) crawler ....processor



Raspberry Pi Image 만들기
  1)OS image : 1)sdcard.org(dsk format) noboos unzip copy
               2)Raspbian stretch(4G) : pihttps://etcher.io/ or Pi Filler startx
               3)Pi Filler/Copier(save snapshot) : http://ivanx.com/raspberrypi/
     - check os version: uname -a

  2)auto-config : commands line , hdmi, region, time, wifi
    - raspi-config
    - wifi cmd;
            ifconfig
            (sudo) iwlist wlan0 scan or sudo iwconfig wlan0 essid network-essid
              sudo nano /etc/wpa_supplicant/wpa_supplicant.conf =>network={ssid="The_ESSID_from_earlier" psk="Your_wifi_password"}
            sudo nano /etc/network/interfaces
                iface wlan0 inet dhcp,wpa-ssid "ssid",wpa-psk "password"
    - bluetooth
            use blueman or sudo apt-get install pi-bluetooth
            bluetoothctl : list, scan, devices, scan on/off, agent on/off, power on/off, remove, quit
  3)install without monitor and keyboard :

SSH connection
      - find rsp : All raspberry devices MAC addresses started with B8:27:EB.
      - PiFinder : http://ivanx.com/raspberrypi/
                   brew install nmap (or wireshark)
                   arp -na | grep -i b8:27:eb
                   sudo nmap -sP 192.168.1.0/24 | awk '/^Nmap/{ip=$NF}/B8:27:EB/{print ip}'

install script
    - python3 : python-pip libglib2.0-dev bluepy
      3.6: https://gist.github.com/dschep/24aa61672a2092246eaca2824400d37f
        sudo apt-get install build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev
        ./configure, make, sudo make altinstall
        sudo apt-get autoremove, clean
model db record 구조
  1) fingerprints : from_Device, timestamp, rssi, mac_, interval, type
  2) device: location,
  3) zone : contour
