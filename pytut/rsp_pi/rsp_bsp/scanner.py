# wifi and bluetooth scanner

from subprocess import check_output

scanoutput = check_output(["iwlist", "wlan0", "scan"])

for line in scanoutput.split():
  if line.startswith(b"ESSID"):
    ssid = line.split(b'"')[1]
    print(ssid)  #type(ssid) is byte

#pip install wifi (unmaintained)
from wifi import Cell, Scheme

def scanForCells():
    # Scan using wlan0
    cells = Cell.all('wlan0')

    # Loop over the available cells
    for cell in cells:
        cell.summary = 'SSID {} / Quality {}'.format(cell.ssid, cell.quality)

        if cell.encrypted:
            enc_yes_no = '*'
        else:
            enc_yes_no = '()'

        cell.summary = cell.summary + ' / Encryption {}'.format(enc_yes_no)

    return cells


cells = scanForCells()
for cell in cells:
    print(cell.summary)

#cmds: bluetoothctl
#cmds : hciconfig, sudo hciconfig hci0 up, sudo hcitool lescan, systemctl status bluetooth

#sudo apt-get install python-pip libglib2.0-dev bluepy sudo blescan
from bluepy import btle
#from bluepy.btle import UUID, Peripheral

#sudo apt-get install bluetooth
#sudo apt-get install bluez
#sudo apt-get install python-bluez
