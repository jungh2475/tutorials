
#http://pbpython.com/pathlib-intro.html
#파일목록을 pandas df로 출력하는 프로그램, 편리함

import pandas as pd
from pathlib import Path
import time

p = Path("/media/chris/KINGSTON/data_analysis")
all_files = []
for i in p.rglob('*.*'):
    all_files.append((i.name, i.parent, time.ctime(i.stat().st_ctime)))

columns = ["File_Name", "Parent", "Created"]
df = pd.DataFrame.from_records(all_files, columns=columns)

df.head()
