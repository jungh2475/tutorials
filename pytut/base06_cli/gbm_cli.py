import click

#@click.command()
'''
    @click.group()
    def cli():
    @click.command()
    cli.add_command(func_name)


    @click.argument('arg')
    @click.option('-t','-token', envvar='gb_token', default='world', help='haha')

    cli install
    1) shebang #!/bin/env python3 => mv xxx.py xxx, add_to PATH
    2)setup tool


'''
@click.group()
def main():
    print("welcome to grid Blossom cli")
    pass


'''
 subcommands : list, add, remove, update, show
  - node
  - function
  - workflow
  - assignment
  - user
  - domain
  - config (user, )
  - whoami

'''
@main.command()
@click.argument('arg2')
def config(arg2):
    print('hello world 2 config ' + arg2)

@main.command()
@click.argument('arg2')
def node(arg2):
    print('hello world node ' + arg2)
    if(arg2=='create'):
        pass

@main.command()
@click.argument('arg2')
def function(arg2):
    print('hello world function ' + arg2)


def node_create(config):
    import node
    node1=Node(config)
    node1.init()

# https://www.bnmetrics.com/blog/dynamic-import-in-python3
# https://copyninja.info/blog/dynamic-module-loading.html
# pip list
# print (help('modules') )
# import gbm_cli, gbm_cli.assure_imports('requests')
def assure_imports(modulename):
    import sys

    #modulename = 'datetime'
    if modulename not in sys.modules:
        print('You have not imported the {} module'.format(modulename))
        #import modulename
        load_module(modulename)
    return True
# https://copyninja.info/blog/dynamic-module-loading.html
#
def load_module(modulename):
    import sys
    import importlib
    mod = None
    print("importing....")
    #pip install modulename
    #import modulename
    try:
        mod = importlib.import_module(modulename)
    except ImportError:
        print("Failed to load {module}".format(module=modulename),
                     file=sys.stderr)
    return mod

def install_and_import(packagename):
    import importlib
    try:
        importlib.import_module(packagename)
    except ImportError:
        import pippip.main(['install',packagename])
    finally:
        globals()[packagename]=importlib.import_module(packagename)
        #if no such name in global(), create one ....packlist=[]
        #packlist.append(packagename)

if __name__ == "__main__":
    main()
