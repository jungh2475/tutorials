python3 -m pip3 install SomePackage
apt-get install python3-venv
pip3 install virtualenv
which virtualenv
which python3
python3.6 -m venv virtualenv --without-pip
virtualenv ~/example.com/my_project -p /home/example_username/opt/python-3.6.2/bin/python3


sudo add-apt-repository ppa:jonathonf/python-3.6
sudo apt-get update
sudo apt-get install python3.6

cd ~
python3.6 -m venv virtualenv --without-pip
cd virtualenv/
source bin/activate
curl https://bootstrap.pypa.io/get-pip.py | python3
