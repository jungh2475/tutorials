from setuptools import setup

'''
  pip install --editable .
'''

setup(
    name='gridblossom-cli',
    version='1.0',
    py_modules=['gbm_cli'],
    install_requires=['Click','requests'],
    entry_points='''
        [console_scripts]
        gb1=gbm_cli:cli
    ''',

)
