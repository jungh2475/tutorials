

#
'''
    run code from the interpreter
    1)python3 -i foo.py
    2)python>> import filename
    3)exec(open("filename.py").read()) #이것은 if __naim..main도 동작시킴, 위의것은 아님

    javascript node에서는 var fs = require('fs');eval(fs.readFileSync('foo.js').toString())
    아니면 no strict일때는 쉽게 .load xxx.js or node -i -e "$(< yourScript.js)"
    or require('./file.js');
'''

class Content(object):

    def __init__(self,seed):
        #self.active, readAccess, writeAccess
        self.readAccess = seed.user.id
        self.writeAccess = seed.user.id

    def __str__(self):
        #
        str='{}...{}'
        return str.format(self.xx, self.yy)


class Action(Content):
    def __init__(self,seed):
        self.hello ='hihi'
        self.readAccess = seed.user.id
        self.writeAccess = seed.user.id

    def run(self):
        print('hello')



if __name__ == "__main__":

    #seed1={} => cls: A = type('A', (object,), {}) type(name, bases, dict) -> a new type
    '''
    쓰기 편하기는 unitest.mock.Mock을 쓰는 것이 제일 편함
        1)A = type("A", (), {})
        x = A()

        user=type('',(),{})()
        user.id=23
        seed1=type('',90,{})()
        seed1.user=user

        body = dict(__doc__='docstring', __name__='not_A', __module__='modname')
        cls2 = type('A', (object,), body)
        setattr(obj.a, 'somefield', 'somevalue')

        2)class Object(object):
            pass

        a = Object()
        a.somefield = somevalue

        3)import mock # from unittest.mock import MagicMock
        #from unittest.mock import Mock
        obj = mock.Mock()
        obj.user.id = 5

    '''
    from unittest.mock import Mock
    seed1 = Mock()
    seed1.user.id = 23
    action1 = Action(seed1)
    action1.run()
    print('hello from main run')
    pass
