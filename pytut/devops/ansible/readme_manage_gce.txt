https://medium.com/vimeo-engineering-blog/orchestrating-gce-instances-with-ansible-d825a33793cd
pip install ansible  or (in ubuntu, sudo apt-get install ansible after sudo apt-add-repository ppa:ansible/ansible)
(or in mac osx , brew install ansible)
ansible --version
ansible localhost -m ping
ansible localhost -a 'uname -a'

ssh-keygen -t rsa -b 4096 -C “tushar@tushar-VirtualBox”
apt-get install sshpass
ssh-copy-id root@192.168.43.181
ansible all -m ping

pip install apache-libcloud
mkdir -p ~/git/ansible/inventory
git clone https://github.com/ansible/ansible
cp ansible/contrib/inventory/gce.py ~/git/ansible/inventory/
cp ansible/contrib/inventory/gce.ini ~/git/ansible/
export GCE_INI_PATH=~/git/ansible/gce.ini   #gce.ini
~/git/ansible/inventory/gce.py — list
#testing only : ansible -i ~/git/ansible/inventory tag_one -m ping
  instance-1 | SUCCESS => {
  “changed”: false,
  “ping”: “pong”
  }
-----------------
By default the inventory file is /etc/ansible/hosts . If you want to use another inventory file you can specify it during execution by using -i <path of inventory file>.
ansible-playbook -i inventory.ini install_LAMP.yml
/etc/ansible/ansible.cfg =>inventory = /etc/ansible/newhost


https://www.jeffgeerling.com/blog/creating-custom-dynamic-inventories-ansible

동적으로 host inventory 관리하는 법중에는 pxssh python script로 하는 것도 있다
/etc/ansible/hosts
mail.example.com

[webservers]
foo.example.com
bar.example.com:5309

[dbservers]
one.example.com
two.example.com
three.example.com

[targets]

localhost              ansible_connection=local
other1.example.com     ansible_connection=ssh        ansible_user=mpdehaan
other2.example.com     ansible_connection=ssh        ansible_user=mdehaan

----------------------
ansible all -i my-inventory-script -m ping
./inventory.py --list
ansible all -i inventory.py -m ping

json from
{
    "group": {
        "hosts": [
            "192.168.28.71",
            "192.168.28.72"
        ],
        "vars": {
            "ansible_ssh_user": "johndoe",
            "ansible_ssh_private_key_file": "~/.ssh/mykey",
            "example_variable": "value"
        }
    },
    "_meta": {
        "hostvars": {
            "192.168.28.71": {
                "host_specific_var": "bar"
            },
            "192.168.28.72": {
                "host_specific_var": "foo"
            }
        }
    }
}

----------------------
// Ansible-INI host file
jumper ansible_port=5555 ansible_host=192.0.2.50
aws_host          ansible_ssh_private_key_file=/home/example/.ssh/aws.pem
freebsd_host      ansible_python_interpreter=/usr/local/bin/python


instead of host file(/etc/ansible/hosts) add inventory dynamically/programmically
vars:
# add host to group 'just_created' with variable foo=42
- add_host:
    name: "{{ ip_from_ec2 }}"
    groups: just_created
    foo: 42

# add a host with a non-standard port local to your machines
- add_host:
    name: "{{ new_ip }}:{{ new_port }}"

# add a host alias that we reach through a tunnel (Ansible <= 1.9)
- add_host:
    hostname: "{{ new_ip }}"
    ansible_ssh_host: "{{ inventory_hostname }}"
    ansible_ssh_port: "{{ new_port }}"

# add a host alias that we reach through a tunnel (Ansible >= 2.0)
- add_host:
    hostname: "{{ new_ip }}"
    ansible_host: "{{ inventory_hostname }}"
    ansible_port: "{{ new_port }}"


#######################################################
http://docs.ansible.com/ansible/latest/guide_gce.html
http://docs.ansible.com/ansible/latest/gce_module.html
https://cloud.google.com/solutions/google-compute-engine-management-puppet-chef-salt-ansible

ansible with apache-libcloud (pip install apache-libcloud)
# ansible modules : gce, gce_pd	persistent disk, gce_lb	load-balancer
# run: ansible-playbook -i inventory.ini gce-playbook.yml
# Example Playbook :

- name: Compute Engine Instance Examples
  hosts: localhost
  vars:
    service_account_email: "your-sa@your-project-name.iam.gserviceaccount.com"
    credentials_file: "/path/to/your-key.json"
    project_id: "your-project-name"
  tasks:
    - name: create multiple instances
      # Basic provisioning example.  Create multiple Debian 8 instances in the
      # us-central1-a Zone of n1-standard-1 machine type.
      gce:
        instance_names: test1,test2,test3
        zone: us-central1-a
        machine_type: n1-standard-1
        image: debian-8 #or image_family: my-base-image
        state: present
        service_account_email: "{{ service_account_email }}"
        credentials_file: "{{ credentials_file }}"
        project_id: "{{ project_id }}"
        metadata : '{ "startup-script" : "apt-get update" }'
        disk_size: 32
        network: foobar-network
        subnetwork: foobar-subnetwork-1
      register: gce

    - name: Save host data
      add_host:
        hostname: "{{ item.public_ip }}"
        groupname: gce_instances_ips
      with_items: "{{ gce.instance_data }}"

    - name: Wait for SSH for instances
      wait_for:
        delay: 1
        host: "{{ item.public_ip }}"
        port: 22
        state: started
        timeout: 30
      with_items: "{{ gce.instance_data }}"

    - name: Configure Hosts
      hosts: gce_instances_ips
      become: yes
      become_method: sudo
      roles:
        - my-role-one
        - my-role-two
      tags:
        - config

    - name: delete test-instances
      # Basic termination of instance.
      gce:
        service_account_email: "{{ service_account_email }}"
        credentials_file: "{{ credentials_file }}"
        project_id: "{{ project_id }}"
        instance_names: "{{ gce.instance_names }}"
        zone: us-central1-a
        state: absent
      tags:
        - delete

################ gce inventory plugin
inventory script gce.py algon with contrib/inventory/gce.ini
./gce.py --list

$ GCE_INI_PATH=~/.gce.ini ansible all -i gce.py -m setup
hostname | success >> {
  "ansible_facts": {
    "ansible_all_ipv4_addresses": [
      "x.x.x.x"
    ],
