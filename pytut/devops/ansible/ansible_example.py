
#pip install ansible or brew install ansible
'''
inventory file
/etc/ansible/hosts
192.0.2.50

[webservers]
aserver.example.org

[dbservers]
bserver.example.org

[atlanta]
host1 http_port=80 maxRequestsPerChild=808
host2 http_port=303 maxRequestsPerChild=909

------------------

$ ssh-agent bash
$ ssh-add ~/.ssh/id_rsa

ansible all -m ping
ansible all -m ping -u bruce --sudo
ansible localhost -m ping -e 'ansible_python_interpreter="/usr/bin/env python"'

/etc/ansible/ansible.cfg or ~/.ansible.cfg

-------------------
playbook examples : ansible-playbook playbook.yml -f 10

test.yml
            - hosts: sample_group_name
  tasks:
    - name: just an uname
      command: uname -a

playbook.yml
- hosts: webservers
  vars:
    http_port: 80
    max_clients: 200
  remote_user: root
  tasks:
  - name: ensure apache is at the latest version
    yum: name=httpd state=latest
  - name: write the apache config file
    template: src=/srv/httpd.j2 dest=/etc/httpd.conf
    notify:
    - restart apache
  - name: ensure apache is running (and enable it at boot)
    service: name=httpd state=started enabled=yes
  handlers:
    - name: restart apache
      service: name=httpd state=restarted

see https://gist.github.com/nod/66f1569ebb0d878748fe

'''


#!/usr/bin/python
import ansible.runner
import ansible.playbook
import ansible.inventory
from ansible import callbacks
from ansible import utils
import json

# the fastest way to set up the inventory

# hosts list
hosts = ["10.11.12.66"]
# set up the inventory, if no group is defined then 'all' group is used by default
example_inventory = ansible.inventory.Inventory(hosts)

pm = ansible.runner.Runner(
    module_name = 'command',
    module_args = 'uname -a',
    timeout = 5,
    inventory = example_inventory,
    subset = 'all' # name of the hosts group
    )

out = pm.run()

print json.dumps(out, sort_keys=True, indent=4, separators=(',', ': '))


-----------------------
## first of all, set up a host (or more)
example_host = ansible.inventory.host.Host(
    name = '10.11.12.66',
    port = 22
    )
# with its variables to modify the playbook
example_host.set_variable( 'var', 'foo')

## secondly set up the group where the host(s) has to be added
example_group = ansible.inventory.group.Group(
    name = 'sample_group_name'
    )
example_group.add_host(example_host)

## the last step is set up the invetory itself
example_inventory = ansible.inventory.Inventory()
example_inventory.add_group(example_group)
example_inventory.subset('sample_group_name')

# setting callbacks: plugins enable you to hook into Ansible events for display or logging
# http://docs.ansible.com/ansible/latest/dev_guide/developing_plugins.html verbose=0
stats = callbacks.AggregateStats()
playbook_cb = callbacks.PlaybookCallbacks(verbose=utils.VERBOSITY)
runner_cb = callbacks.PlaybookRunnerCallbacks(stats, verbose=utils.VERBOSITY)

# creating the playbook instance to run, based on "test.yml" file
pb = ansible.playbook.PlayBook(
    playbook = "test.yml",
    stats = stats,
    callbacks = playbook_cb,
    runner_callbacks = runner_cb,
    inventory = example_inventory,
    check=True
    )

# running the playbook
pr = pb.run()

# print the summary of results for each host
print json.dumps(pr, sort_keys=True, indent=4, separators=(',', ': '))
