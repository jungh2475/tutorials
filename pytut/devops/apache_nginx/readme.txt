

Redirect and Rewrite는 바뀐 주소를 browser가 알게 된다.
그러면 gCloud안으로 직접 접속이 안될수도 있다 .
Proxy에서는 server is completely hidden for the user.[301] 대신에 [P]proxy로 대응하면 어떨ㄲ?

static.kewtea.com/journal/img이런식으로 정리해줄 필요가 있다
http-proxy static file cache를 설정하는 법?

Redirect / http://www.foobar.com/
Alias /images/ "/var/www/images/"


mod_rewrite

  RewriteEngine on
  RewriteCond %{HTTP_HOST}   !^$
  RewriteRule   ^/~([^/]+)/?(.*)    /u/$1/$2  [P]
  RewriteRule   ^foo\.html$             foo.day.html

  RewriteRule ^/mysite/(.*) http://www.example.com:8080/mysite/$1 [P]
  <Location /mysite/>
    ProxyPass http://www.example.com:8080/
    ProxyPassReverse http://www.example.com:8080/
  </Location>

  기타 규칙들
  RewriteCond %{HTTP_USER_AGENT}  ^Mozilla/3.*
  RewriteCond  %{REQUEST_URI}  !^$

mod_proxy

  ProxyRequests On
  ProxyPass / https://www.akadia.com/
  ProxyPassReverse / https://www.akadia.com/

  #option1- selective url
  ProxyPassMatch ^/(.*) http://granny-server:8181/$1
  ProxyPassReverse / http://granny-server:818


mod_header


<VirtualHost *:80>

      ServerName example.com
      ServerAlias www.mydomain.com

      Alias /images/ "/var/www/images/"

      RewriteEngine On

      RewriteCond %{HTTP:UserId} !^(ab123|ze678|gt465)$  #%{HTTP:header}
      #RewriteCond %{HTTP:X-Forwarded-Port} !=443
      #RewriteCond %{HTTP:X-Forwarded-For} !=123.45.67.89
      RewriteRule .* https://%{SERVER_NAME}%{REQUEST_URI} [R=301,L]
      #RewriteRule ^ /maintenance.html [R=302]

      # proxy to the Jellyfish server (ignoring images)
      RewriteCond  %{REQUEST_URI}  !^/(images)(.*)$
      RewriteRule  ^(/.*)$         http://app-server:8181/jellyfish$1  [P]
      ProxyPassReverse  /          http://app-server:8181/jellyfish/

      <Directory "/var/www/images">
            Options Indexes MultiViews FollowSymLinks
            AllowOverride None
            Order allow,deny
            Allow from all
      </Directory>
