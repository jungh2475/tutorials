
https://www.the-swamp.info/blog/configuring-gcloud-multiple-projects/

sudo로 맨처음에 설치해야 경로에 gcloud가 등록된다. 그리고 다음을 실행
sudo ./install.sh
#################
gcloud init
gcloud components update && gcloud components install beta

#### add service account json file
gcloud auth activate-service-account --key-file

gcloud config list  #현재 세팅
gcloud config configurations list #변경가능한 세팅들
gcloud config configurations activate default  #config 변경
gcloud compute config-ssh
gcloud compute zones list

gcloud compute images list
gcloud compute instances list
gcloud compute instances describe example-instance

gcloud compute instances create testvm1 --machine-type f1-micro --image-project ubuntu-os-cloud  --image-family ubuntu-1404-lts
https://cloud.google.com/sdk/gcloud/reference/compute/instances/create
#--custom-cpu 4 --custom-memory 5376MB
gcloud compute disks create [DISK_NAME] --source-snapshot [SNAPSHOT_NAME]
gcloud compute machine-types list, f1-micro        us-central1-f           1     0.60
#n1-standard-1, asia-east1
gcloud compute instances set-machine-type example-instance \
      --zone us-central1-b --machine-type n1-standard-4

gcloud compute ssh {instance} #from $USER
ssh -i [PATH_TO_PRIVATE_KEY] [USERNAME]@[EXTERNAL_IP_ADDRESS]
