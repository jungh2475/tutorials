

[1. development ]
설치하고 다시 지우고 하는 환경.....
  1-1) unit test in mac terminal: node, spring, pytest
  1-2) server testing in mac vbox : (pxssh) => vagrant  (3개이상 서버가 어려움)
  1-3) gCloud에서 개발 테스트 할 경우 : pxssh => vagrant provision=gcloud

[2. production deploy 경우 ]
기존 설치된 instances 관리 중심
  2-1) 개발자 osx desk :  pxssh scripts ( ansible => )
  2-2) gcloud console :  일일히 ssh 접속하여 설치 및 확인
        -> log report, script(measure/backup/restore/repair/restart/reinstall),


[3. multi-machine docker container 만들기]

  3-1)Docker Image 만들기 : 1)kew_wf_spring_1.2, 2)kew_py_data_1.3, 3)kew_registry_1.1 4)kew_db_es_1.1
  3-2)Container로 배포+실행 : pxssh -> docker run [img_name]  -> ansible로 추가 설정(localhost상대로)  -> 서비스 시작 확인 
  3-3)microservice_registry에 알리거나/등록해서 서비스를 사용함.





1.배울것
python pexpect, pxssh, fabric
ansible https://brunch.co.kr/@jiseon3169ubie/2

2.하는일
(vagrant-box)
  base os image
(vagrant provision with gcloud or aws)

(ansible)
  시스템설치: 기본 패키지: git, python3(pip), php, java(set JAVA_HOME), apache, (sftp, wordpress, node, mysql)
  시스템 원격 restart
  서버상황/file 로그가져오기(ubunutu, apache, mysql, spring)
  파일 backup: wordpress mysql 및 upload folder(find -mtime) -> google cloud storage (cold line)
  (proxy spring running)

(docker)
  (proxy spring running)
  spring/python sw runner docker image

3.시스템 구성
1) mac -> vagrant vm(ubuntu)
2) vagrant vm(3),  admin, webfront, db,  ssh로 admin안에서 3개 실행
3) google cloud : 3개
  a)mac에서 gcloud sdk로 admin에 ssh해서 스크립트 실행(webfront db 통제)
  b)mac에서 gcloud sdk로 admin에 ssh하지말고 스크립트 실행(webfront db 통제)
