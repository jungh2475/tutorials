[INSTALLS]
docker Dockerfile -> image -> run : container,  .....service docker stop/start

  install on ubuntu
    uname -a.... or lsb_release -a

    #Docker Engine has been renamed to Docker Community Edition
    #https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/#install-docker-ce, Docker EE is Docker CE with certification
    sudo apt-get remove docker docker-engine docker.io
    sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt-get update
    sudo apt-get install docker-ce
    #end of docker ce install

    sudo apt-get update
    sudo apt-get install apt-transport-https ca-certificates
    #add the new GPG key
    sudo apt-key ads \ --keyserver hkp://ha.pool.sks-keyservers.net:80 \ --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
    #apt package manager https://apt.dockerproject.org/repo
    echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main” | sudo tee /etc/apt/sources.list.d/docker.list
    apt-get update
    apt-cache policy docker-engine
    apt-get update
    sudo usermod -aG docker $(whoami)
    apt-get install -y lsb-release
    sudo apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual
    sudo apt-get install –y docker-engine
    # or sudo apt-get install docker.io, sudo ln -sf /usr/bin/docker.io /usr/local/bin/docker
    # sudo service docker start

install on mac

install on vagrant/vm=ubuntu/ansible

test with
docker info
docker version


[run docker - basic]

docker images
sudo docker pull jenkins  # vs docker rmi ImageID
docker inspect jenkins  # or docker inspect apacheweb1 | grep IPAddress
sudo docker run -p 8080:8080 -p 50000:50000 jenkins
#build
docker build -t centos/apache:v1 .  #t:tagName
# docker run --rm hello-world
#sudo docker run centos –it /bin/bash
sudo docker run centos tail -f /dev/null #계속 동작하게 하려고.....(절대 실행 종료 없음))
sudo docker run -it -d -p 8080:80 --name test_instance ubuntu -it /bin/bash
docker run -it ubuntu:latest  /bin/bash
docker ps
docker ps -a
docker history ImageID
docker stop(kill) ContainerID ,docker rm ContainerID
docker stats ContainerID, docker pause(unpause) ContainerID
docker attach ContainerID
docker logs -f ContainerID #tail -f

sudo nsenter –m –u –n –p –i –t 2978 /bin/bash
docker build  -t ImageName:TagName dir


[docker commands]
docker build -t friendlyname .  # Create image using this directory's Dockerfile
docker run -p 4000:80 friendlyname  # Run "friendlyname" mapping port 4000 to 80
docker run -d -p 4000:80 friendlyname         # Same thing, but in detached mode
docker container ls                                # List all running containers
docker container ls -a             # List all containers, even those not running
docker container stop <hash>           # Gracefully stop the specified container
docker container kill <hash>         # Force shutdown of the specified container
docker container rm <hash>        # Remove specified container from this machine
docker container rm $(docker container ls -a -q)         # Remove all containers
docker image ls -a                             # List all images on this machine
docker image rm <image id>            # Remove specified image from this machine
docker image rm $(docker image ls -a -q)   # Remove all images from this machine
docker login             # Log in this CLI session using your Docker credentials
docker tag <image> username/repository:tag  # Tag <image> for upload to registry
docker push username/repository:tag            # Upload tagged image to registry
docker run username/repository:tag                   # Run image from a registry



[docker services/swarm/stacks]
docker compose : docker-compose --project-name app-test -f docker-compose.yml up
//docker-compose.yml
version: '2'
services:
  ...
  redis:
    image: redis:3.2-alpine
    volumes:
      - redis_data:/data
volumes:
  redis_data:

//docker volume ls
#########
wordpress : 1)mysql, 2)wp-upload files
(docker run -v /host/dir:container/dir)
docker run -d --name wordpress-some -v /home/web/wp-one:/var/www:ro wordpressImg
---------------------------
//passing password (환경변수 주입 $param)
docker run --name mysql-some -e MYSQL_ROOT_PASSWORD=pw -e VARAMX=36476 -d mysql

data volume container : FROM ubuntu:latest VOLUME ["/var/www"]  //source
docker run -d --name data-container our-data-container tail -f /dev/null
docker run -d --name wordpress-some --volumes-from data-container  --link mysql-some:mysql
           -d -p 8080:80 wordpress


########  docker attach then detach without shut down shell
docker attach 665b4a1e17b6
docker exec -i -t imgName /bin/sh
exit
#만약에 죽었다면 다시 시작  docker start foo, or try Ctrl-p + Ctrl-q ^P^Q
