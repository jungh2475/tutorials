ansible-playbook -i hosts playbook1.yml
ansible-playbook -i hosts --limit "Europe" playbook1.yml
ansible 127.0.0.1 -m ping -i ansible/hosts.ini
ansible all -i "192.168.33.100," -a 'uptime'
ansible all --list-hosts
ansible -i hosts all -m ping -u vagrant
ansible -i hosts all -m copy -a "src=test.sh dest=/root/" -u root
#.....-m file -a "dest=/root/ansible mode=755 owner=adminsupport group=adminsupport state=directory" -u root


ansible-playbook -l host_subset playbook.yml

ssh vagrant@192.168.100.100 -i /your/user/directory/.vagrant.d/insecure_private_key

vagrant ansible vs ansible local

  Vagrant Ansible provisioner allows you to provision the guest using Ansible playbooks by executing ansible-playbook from the Vagrant host
  The Vagrant Ansible Local provisioner allows you to provision the guest using Ansible playbooks by executing ansible-playbook directly on the guest machine.



  [ansible local]
  Vagrant.configure("2") do |config|
    # Run Ansible from the Vagrant VM
    config.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "playbook.yml"
    end
  end




##########################
 ansible tutorials
##########################


1)initial setup environments: inventory : add/remove servers
2)install sw & configure (prepare machine first time)
3)shutdown and restart machines
4)sw updating : CI
5)install tracers via pxssh


1)inventory
  - add_host:
      name: "{{ ip_from_ec2 }}"
      groups: just_created
      foo: 42


3)ansible to reboot/shutdown
