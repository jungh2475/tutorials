#!/bin/bash

#   sudo passwd ubuntu  #cut -d: -f1 /etc/passwd
#   adduser kewtea
#   groups kewtea
#   visudo
#   usermod -aG sudo kewtea #/etc/sudoers ->root    ALL=(ALL:ALL) ALL

sudo apt-get update
apt-get install -y apache2
apt-get install -y git
sudo apt-get install -y zip
sudo apt-get install -y unzip
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk version
sdk install groovy
sdk install java
sdk install maven
sudo add-apt-repository ppa:jonathonf/python-3.6
sudo apt-get update
sudo apt-get install -y python3.6
#sudo update-alternatives --config python3
python3 -V
sudo ln -s python3 /usr/bin/python3.6
python3 -m venv venv01
source venv01/bin/activate
#   cd ...git init ...git add user...git remote add origin..git pull origin master
