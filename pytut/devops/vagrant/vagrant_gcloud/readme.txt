
http://blog.nguyenvq.com/blog/2016/03/07/using-vagrant-to-scale-data-science-projects-with-google-cloud-compute/
https://realguess.net/2015/09/07/setup-development-environment-with-vagrant-on-google-compute-engine/
https://github.com/mitchellh/vagrant-google
in the API Console, go to Credentials subsection, and click Create credentials -> Service account key...

sudo apt-get install -y build-essential
vagrant plugin install vagrant-google


########## gcloud init
google_json_key.json
#done via google metadata server: to add public key to the instance
echo -n 'userName:' > /tmp/id_rsa.pub
cat ~/.ssh/id_rsa.pub >> /tmp/id_rsa.pub # e.g. chao:ssh-rsa AAAAB3... chao@ubuntu

gcloud compute project-info add-metadata --metadata-from-file \
  sshKeys=/tmp/id_rsa.pub
vagrant box add gce https://github.com/mitchellh/vagrant-google/raw/master/google.box
vagrant box list


//Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "gce"
  config.vm.box_version = "0.1.0"
  config.vm.provider :google do |google, override|
    google.google_project_id = "PROJECT_ID"
    google.google_client_email = "PROJECT_ID_SERVICE_ACCOUNT@developer.gserviceaccount.com"
    google.google_json_key_location = "~/path/to/gcloud.json"

    google.name = "devel" # Define the name of the instance.
    google.zone = "asia-east1-c"
    google.machine_type = "n1-standard-2"
    # `$ gcloud compute images list`.
    google.image = "ubuntu-1404-trusty-v20150901a"
    google.disk_name = "disk-ds" # ds for data science
    google.disk_size = "10"
    override.ssh.username = "chao"
    override.ssh.private_key_path = "~/.ssh/id_rsa"
  end
  config.vm.provision :shell, path: "provision.sh"
end

//provision.sh
#! /usr/bin/env bash

touch /tmp/foo # whoami?

# configure mail so I could communicate status updates
# http://askubuntu.com/questions/12917/how-to-send-mail-from-the-command-line,
# http://www.binarytides.com/linux-mail-command-examples/
# http://superuser.com/questions/795883/how-can-i-set-a-default-account-in-heirloom-mailx
cat > /tmp/nail.rc <<EOF
set smtp-use-starttls
set smtp-auth=login
set smtp=smtp://smtp.gmail.com:587
set smtp-auth-user=MY_GMAIL@gmail.com
set smtp-auth-password="MY_GMAIL_PW"
EOF
sudo su
cat /tmp/nail.rc >> /etc/nail.rc
exit
rm /tmp/nail.rc

# configure screen
cat > ~/.screenrc <<EOF
escape ^lL
bind c screen 1
bind 0 select 10
screen 1
select 1
autodetach on
startup_message off
EOF

# Software installation
sudo su
echo "deb http://cran.rstudio.com/bin/linux/ubuntu trusty/" > /etc/apt/sources.list.d/r.list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9
apt-get update
apt-get -y install r-base-dev libcurl4-openssl-dev libssl-dev git
apt-get -y install heirloom-mailx # setup mail
exit # sudo

# VW
sudo apt-get -y install libtool libboost1.55-*

# Java
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get -y install oracle-java7-installer

#########################

gcloud compute instances set-disk-auto-delete gce-instance --no-auto-delete --disk disk-ds # don't delete root disk https://cloud.google.com/compute/docs/disks/persistent-disks#updateautodelete
vagrant destroy --force
gcloud compute images create image-ds --source-disk disk-ds --source-disk-zone us-central1-b
gcloud compute instances set-disk-auto-delete gce-instance --auto-delete --disk disk-ds
vagrant destroy # now disk is also destroyed, only image is left.

#########################
vagrant up --provider=google
vagrant ssh
#vagrant destroy

# paste the following
mailaddr=MY_CONTACT_EMAIL
projdir=/vagrant/my_project
logdir=/vagrant/my_project/log
mkdir -p $logdir
