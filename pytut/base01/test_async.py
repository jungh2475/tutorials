import asyncio
#http://lucumr.pocoo.org/2016/10/30/i-dont-understand-asyncio/

async def foo():
    print("async called")
    await asyncio.sleep(1)


loop = asyncio.get_event_loop()
loop.run_until_complete(foo())
loop.close()
print("completed")
