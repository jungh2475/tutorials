from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed
#from concurrent.futures import ProcessPoolExecutor
'''
    io작업(file,network access)는 multithread를 사용하고, 계산이 많은 것은 multiprocess(multi-cpu일경우)를 쓴다
    abs_Executor.map, submit, shutdown,
    classic old 방법이 좋은 이유는 sync_function을 그대로 쓸수 있기 때문임.
    future = executor.submit(function, input, timeout) , Future instances are created by Executor.submit()


'''



executor = ThreadPoolExecutor(max_workers=3)



#http://hamait.tistory.com/828
#http://masnun.com/2016/03/29/python-a-quick-introduction-to-the-concurrent-futures-module.html
import urllib.request

URLS = ['http://www.foxnews.com/',
        'http://www.cnn.com/',
        'http://europe.wsj.com/',
        'http://www.bbc.co.uk/',
        'http://kewtea.com/']

# Retrieve a single page and report the URL and contents
def load_url(url, timeout):
    with urllib.request.urlopen(url, timeout=timeout) as conn:
        return conn.read()

import requests

def load_url2(url,time):
    headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'}
    html=""
    try:
        r = requests.get(url, headers=headers, timeout=10)
        if r.status_code == 200:
            html = r.text
    except Exception as ex:
        print(str(ex))
    finally:
        pass
    return html #len(html)

def trim_html(html,pattern):
    return html

def save_mysql(inputs):
    pass

# We can use a with statement to ensure threads are cleaned up promptly
with executor:
    # Start the load operations and mark each future with its URL
    future_to_url = {executor.submit(load_url, url, 60): url for url in URLS}
    for future in as_completed(future_to_url):
        url = future_to_url[future] ###### executor.submit(func,param,timeout)
        try:
            data = future.result()
        except Exception as exc:
            print('%r generated an exception: %s' % (url, exc))
        else:
            print('%r page is %d bytes' % (url, len(data)))
