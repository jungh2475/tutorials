import signal
import sys
import asyncio
import aiohttp
import json

loop = asyncio.get_event_loop()
client = aiohttp.ClientSession(loop=loop)

#http://stackabuse.com/python-async-await-tutorial/

class AsyncBase:

    def __init__(self):
        self.name = 'tom'

    async def get_json(client, url):
        async with client.get(url) as response:
            assert response.status == 200
            return await response.read()

    async def get_reddit_top(subreddit, client):
        data1 = await get_json(client, 'https://www.reddit.com/r/' + subreddit + '/top.json?sort=top&t=day&limit=5')

        j = json.loads(data1.decode('utf-8'))
        for i in j['data']['children']:
            score = i['data']['score']
            title = i['data']['title']
            link = i['data']['url']
            print(str(score) + ': ' + title + ' (' + link + ')')

            print('DONE:', subreddit + '\n')

    def signal_handler(signal, frame):
        loop.stop()
        client.close()
        sys.exit(0)

if __name__ == "__main__":

    aBase = new AsyncBase()
    signal.signal(signal.SIGINT, signal_handler)

    asyncio.ensure_future(aBase.get_reddit_top('python', client))
    #vs loop.run_until_complete(speak_async()), 루프를 계속 돌리고 올때 마다 처리하던가, 한꺼번에 다 넣어서 맨 나중에 다 같이 처리하던가....
    asyncio.ensure_future(get_reddit_top('programming', client))
    asyncio.ensure_future(get_reddit_top('compsci', client))
    loop.run_forever()

    '''
        my_event_loop = asyncio.get_event_loop()
    try:
        print('task creation started')
        task_obj = my_event_loop.create_task(my_task(seconds=2))
        my_event_loop.run_until_complete(task_obj)
    finally:
        my_event_loop.close()

    print("The task's result was: {}".format(task_obj.result()))


        https://www.blog.pythonlibrary.org/2016/07/26/python-3-an-intro-to-asyncio/

        async def download_coroutine(session, url):
            with async_timeout.timeout(10):
                async with session.get(url) as response:
                    filename = os.path.basename(url)
                    .............
                    return await response.release()


        async def main(loop):
            urls=[]
            async with aiohttp.ClientSession(loop=loop) as session:
                tasks = [download_coroutine(session, url) for url in urls]
                await asyncio.gather(*tasks)

        loop = asyncio.get_event_loop()
        loop.run_until_complete(main(loop))

    '''

    '''
        http://djangostars.com/blog/asynchronous-programming-in-python-asyncio/
        https://docs.python.org/3/library/asyncio-task.html
        - self.loop = asyncio.get_event_loop()
        - addTasks(async_def_method) : self.tasks + >asyncio.ensure_future(async_def_method)
        - start : loop.run_until_complete(asyncio.wait(self.tasks))
    '''

    '''
        class Task(futures.Future):
            http://djangostars.com/blog/asynchronous-programming-in-python-asyncio/

        start = time.time()
        loop = asyncio.get_event_loop()

        tasks = [
            asyncio.ensure_future(factorial("A", 3)),
            asyncio.ensure_future(factorial("B", 4)),
        ]
        loop.run_until_complete(asyncio.wait(tasks))
        loop.close()

        end = time.time()
    '''
