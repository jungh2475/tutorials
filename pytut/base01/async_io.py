
#http://cheat.readthedocs.io/en/latest/python/asyncio.html
#https://medium.com/python-pandemonium/asyncio-coroutine-patterns-beyond-await-a6121486656f
import asyncio
import argparse
import logging
from urllib.parse import urlparse, parse_qs
from datetime import datetime

import aiohttp
import async_timeout

async def greet(msg,delay=1):
    await asyncio.sleep(delay)
    print(msg)
    await return 1

async def fetch(session, url):
    """Fetch a URL using aiohttp returning parsed JSON response.
    As suggested by the aiohttp docs we reuse the session.
    """
    global fetch_counter
    with async_timeout.timeout(FETCH_TIMEOUT):
        fetch_counter += 1
        async with session.get(url) as response:
            return await response.json()


async def main()
    task = asyncio.ensure_future()
    #future_tasks
    result = await asyncio.gather(*future_tasks)
    task.cancel()


loop = asyncio.get_event_loop()
future_result=loop.run_until_complete(greet("hello", 3))
#asyncio.ensure_future()는 병렬처리 모듈인 concurrent.futures의 Executor.submit()과 동일한 역할
#future_tasks = [asyncio.ensure_future(//function(var)// for var in varList]
loop.close()
