import asyncio
#import aiohttp

class AsyncBase2:

    def __init__(self):
        self.name = "myname"
        print("initialised")
        self.loop = asyncio.get_event_loop()
        self.tasks = []

    def close(self):
        self.loop.close()

    def addTask(self,task):
        print("task added")
        self.tasks.append(task)

    async def requests(self,url):
        print("requests called1")
        return await "hihi"+url

    async def runAll(self):
        await asyncio.sleep(random() * 3)
        return await self.loop.run_until_complete(self.tasks)  #각 tasks들의 결과들이 list로 출력된다

async def requests(url):
    print("requests called2")
    await asyncio.sleep(random() * 3)
    return await "hihi"+url

def sync_requests(url):
    print("requests called3")
    return "hihi"+url

if __name__ == "__main__":
    myObj=AsyncBase2()
    print("HIHIHI")
    #myObj.addTask(myObj.requests("iam"))
    myObj.addTask(requests("iamwill"))
    #myObj.requests("iam")

    results = myObj.runAll()
    myObj.close()
    #print(results)
    pass
