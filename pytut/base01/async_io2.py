#http://cheat.readthedocs.io/en/latest/python/asyncio.html
#https://soooprmx.com/archives/6882
from asyncio import loop

def fn():
  pass
value="123"

loop = asyncio.get_event_loop()
#loop = asyncio.new_event_loop()
#asyncio.set_event_loop(loop)

future = loop.create_future()
future.add_done_callback(fn)
future.set_result(value)

async def task_func():
    print('in task_func')
    return 'the result'


#1번 방법
future = loop.create_task(task_func()) #async_func: coroutine
#2번 방법
future = asyncio.ensure_future(coroutine[, loop=loop])
