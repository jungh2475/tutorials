# import requests
import asyncio


class BaseObject(object):
    """Base Object Definition in my project
        - #!/usr/bin/python
        - static variable 은 여기에(ClassName.Variable), 그리고 method는 @staticmethod @classmethod (cls)
        -
    """
    def __init__(self, name):
        """ constructor
        """
        self.name = name  # []


class AsyncObject(object):
    """
    """
    def __init__(self, name):
        """ constructor
        """
        self.name = name
        self.loop = asyncio.get_event_loop()

    #future task style
    async def my_async_me(seconds):
        print('This task is taking {} seconds to complete'.format(seconds))
        await asyncio.sleep(seconds)
        return 'task finished'

    def loadTask(self, async_task):
        try:
            # self.loop.create_task(task)
            self.loop.run_until_complete(async_task)
        finally:
            self.loop.close()
        return "done"

class Looper(object):

    def __init__(self, name):
        """ constructor
        """
        self.name = name

class Crawler:
    pass:



if __name__ == "__main__":
    x = BaseObject('john')
    y = AsyncObject('jane')
