#logger, mail,
#https://mail.python.org/pipermail/python-list/2016-February/703176.html
#https://stackoverflow.com/questions/26270681/can-an-asyncio-event-loop-run-in-the-background-without-suspending-the-python-in
#http://www.curiousefficiency.org/posts/2015/07/asyncio-background-calls.html

class BackgroundTask:
    async def run(self, coro, args, callback=None):
        loop = asyncio.get_event_loop()
        loop.run_in_executor(None, self.task_runner, coro, args, callback)

    def task_runner(self, coro, args, callback):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        fut = asyncio.ensure_future(coro(*args))
        if callback is not None:
            fut.add_done_callback(callback)

        loop.run_until_complete(fut)
        loop.close()

#Usage -

    bg_task = BackgroundTask()
    args = (arg1, arg2 ...)
    callback = my_callback_function
    await bg_task.run(coro, args, callback)
